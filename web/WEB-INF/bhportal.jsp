<%@page import="model.dtos.BhGroupDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.dtos.BhUserDTO"%>
<%@page import="model.beans.AdminToolsBean"%>
<%@page import="model.beans.UserBean"%>
<jsp:useBean id="admintoolsbean" class="model.beans.AdminToolsBean" scope="session"/>




<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Told a cuccot html</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="resources/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <ul>
            <li>
                <a href="admin?userevent=showpage">
                    Felhasználókezelés   
                </a>  
            </li>
            <li>
                <a href="#tab-nav-2">
                    Jelszó módosítás  
                </a> 
            </li>
            <li>
                <a href="admin?userevent=toinsertgroup">
                    Csoport létrehozása
                </a>
            </li>
            <li>
                <a href="#tab-nav-4">
                    Új vizsgaidőpont
                </a>
            </li>
            <li>
                <a href="<%=request.getContextPath()%>/ExamServlet">Fileszerver teszt</a>
            </li>
        </ul>
        <div>
            <% if(admintoolsbean.isCreateUser()){ %>
                <section id="newuser">
                    <h1>Rögzítés</h1>
                    <form name="register" action="admin" method="post" accept-charset="ISO-8859-1">
                        <table>
                            <tr><td>Felhasználónév:</td><td><input type="text" name="userName"></td></tr>
                            <tr>
                                <td colspan="2"><hr></td>   
                            </tr>
                            <tr><td>Vezetéknév:</td><td><input type="text" name="firstName" /></td></tr>
                            <tr><td>Keresztnév:</td><td><input type="text" name="lastName" /></td></tr>
                            <tr><td>Jelszó:</td><td><input type="password" name="password" /></td></tr>
                            <tr><td>Jelszó újra:</td><td><input type="password" name="password1"/></td></tr>
                            <tr><td>Csoport:</td><td>
                                    <select name="group"> 
                                        <% for (int i = 0; i < admintoolsbean.getGroupList().size(); i++) {
                                            String groupName = admintoolsbean.getGroupList().get(i).getName();
                                                if(i==0){%>
                                                <option value="<%=groupName%>"  selected><%=groupName%></option>
                                            <%} else {%>
                                                <option value="<%=groupName%>" ><%=groupName%></option>
                                            <%}
                                        }%>
                                    </select>
                                </td></tr>
                            <tr><td>Jogosultság:</td> 
                                <td>
                                    <select name="right"> 
                                        <% for (int i = 0; i < admintoolsbean.getRightList().size(); i++) {
                                            String accessLevel = model.AccessLevelNames.ACCESS_LEVEL_NAMES[admintoolsbean.getRightList().get(i).getAccessLevel()];
                                            if(i == 0){%>
                                                <option value="<%=accessLevel%>" selected><%=accessLevel%></option>
                                            <%} else {%>
                                                <option value="<%=accessLevel%>" ><%=accessLevel%></option>
                                            <%}
                                        }%>
                                    </select>
                                </td>
                            <!--<tr><td>Email:</td><td><input type="email" name="email"/></td></tr>-->
                            <tr><td>Aktív:</td><td><input type="checkbox" name="active" checked /></td></tr>
                            <tr><td></td><td colspan="2"><input type="submit" value="Felvétel"/></td></tr>
                        </table>
                        <input type="hidden" name="userevent" value="insertuser">            
                    </form>  
                    <hr>
                    <h3><%=admintoolsbean.getMessage()%></h3>                            
                    <a href="admin?userevent=showpage">Mégse</a>
                </section>
            <%} else if(admintoolsbean.isEditUser()){ %>
                <section id="edit">
                    <h1>Módosítás</h1>
                    <form name="register" action="admin" method="post" accept-charset="ISO-8859-1">
                        <table>
                            <%BhUserDTO user = null;
                              for (int i = 0; i < admintoolsbean.getUserList().size() && user==null; i++) {
                                BhUserDTO u = admintoolsbean.getUserList().get(i);
                                if(u.getId() == admintoolsbean.getSelecteUserIndex()){
                                  user = u; 
                                }
                              }
                            %>
                            <tr><td>Felhasználónév:</td><td><input type="text" name="userName"
                                    placeholder="<%=user.getUserName() == null ? "" : user.getUserName() %>" readonly></td></tr>
                            <tr>
                                <td colspan="2"><hr></td>   
                            </tr>
                            <tr><td>Vezetéknév:</td><td><input type="text" name="firstName" value="<%=user.getFirstName()%>"/></td></tr>
                            <tr><td>Keresztnév:</td><td><input type="text" name="lastName" value="<%=user.getLastName() %>"/></td></tr>
                            <tr><td>Jelszó:</td><td><input type="password" name="password" /></td></tr>
                            <tr><td>Jelszó újra:</td><td><input type="password" name="password1"/></td></tr>
                            <tr><td>Csoport:</td><td>
                                    <select name="group"> 
                                        <% for (int i = 0; i < admintoolsbean.getGroupList().size(); i++) {
                                            String groupName = admintoolsbean.getGroupList().get(i).getName();
                                                if(admintoolsbean.getGroupList().get(i).getId() == user.getGroupDTO().getId()){%>
                                                <option value="<%=groupName%>"  selected><%=groupName%></option>
                                            <%} else {%>
                                                <option value="<%=groupName%>" ><%=groupName%></option>
                                            <%}
                                        }%>
                                    </select>
                                </td></tr>
                            <tr><td>Jogosultság:</td> 
                                <td>
                                    <select name="right"> 
                                        <% for (int i = 0; i < admintoolsbean.getRightList().size(); i++) {
                                            String accessLevel = model.AccessLevelNames.ACCESS_LEVEL_NAMES[admintoolsbean.getRightList().get(i).getAccessLevel()];
                                            if(admintoolsbean.getRightList().get(i).getAccessLevel() == user.getRightDTO().getAccessLevel()){%>
                                                <option value="<%=accessLevel%>" selected><%=accessLevel%></option>
                                            <%} else {%>
                                                <option value="<%=accessLevel%>" ><%=accessLevel%></option>
                                            <%}
                                        }%>
                                    </select>
                                    <input type="hidden" name="userevent" value="updateuser">
                                    <input type="hidden" name="uid" value = "<%=user.getId() %>"> 
                                </td>
                            <!--<tr><td>Email:</td><td><input type="email" name="email"/></td></tr>-->
                            <%if(user.getActive() != 0){ %>
                                <tr><td>Aktív:</td><td><input type="checkbox" name="active" checked /></td></tr>
                            <%} else {%>
                                <tr><td>Aktív:</td><td><input type="checkbox" name="active"/></td></tr>
                            <%}%>
                            <tr><td></td><td colspan="2"><input type="submit" value="Módosít"/></td></tr>
                        </table>
                    </form>   
                    <hr>
                    <h3><%=admintoolsbean.getMessage()%></h3>                            
                    <a href="admin?userevent=showpage">Mégse</a>
                </section>
            <% } else if(admintoolsbean.isAddGroup()) { %>    
                <section id="insertgroup">
                    <h1>Csoport hozzáadása</h1>
                    <form name="registergroup" action="admin" method="post" accept-charset="ISO-8859-1">
                        <table>
                            <tr>
                                <td>Új csoport: </td>
                                <td><input type="text" name="groupname"></td>
                                <td></td>
                                <td>
                                    <input type="hidden" name="userevent" value="insertgroup">
                                    <input type="submit" value="Rögzítés"/>
                                </td>
                            </tr>
                        </table>
                        <hr>
                        <h3><%=admintoolsbean.getMessage()%></h3>
                    </form>
                    <a href="admin?userevent=showpage">Mégse</a>
                </section>
            <%} else {%>
                <section id="Userlist">
                    <h1>Felhasználólista</h1>   
                    <table class="admintable">
                        <tr>
                            <td class="admin">Felhasználónév</td>
                            <td class="admin">Vezetéknév</td>
                            <td class="admin">Keresztnév</td>
                            <td class="admin">Csoport</td>
                            <td class="admin">Jogosultság</td>
                            <td class="admin">Aktív</td>
                            <td class="admin">Szerkesztés</td>
                            <!--<td class="admin">Törlés</td>-->
                        </tr>
                        <%for(int i = admintoolsbean.getFirstUserIndex(); i < admintoolsbean.getEndUserIndex(); i++){
                           BhUserDTO user = admintoolsbean.getUserList().get(i);%>
                                <tr>
                                        <td class="admin"><%=user.getUserName()%></td>
                                        <td class="admin"><%=user.getLastName()%></td>
                                        <td class="admin"><%=user.getFirstName()%></td>
                                        <td class="admin"><%=user.getGroupDTO().getName()%></td>
                                        <td class="admin"><%=user.getRightDTO().getAccessLevelName()%></td>
                                        <td class="admin"><%=user.getActive() == 0 ? "nem" : "igen"%></td>
                                        <td class="admin">
                                            <%UserBean userbean = (UserBean) session.getAttribute("userbean");
                                              if(!user.getUserName().equals(userbean.getUserName())) { %> 
                                                <a href="admin?userevent=toedituser&uid=<%=user.getId()%>">Szerkesztés</a>
                                            <%}%>
                                        </td> 
                                        <!--<td class="admin">
                                        <%if(!user.getUserName().equals(userbean.getUserName())) { %>
                                                <a href="admin?userevent=delete&uid=<%=user.getId()%>">Törlés</a>
                                            <%}%>
                                        </td>-->
                                </tr>
                        <%}%>
                    </table>
                    <article id="insert"><a href="admin?userevent=toinsertuser">Új felhasználó felvitele</a></article>
                    <article id="left"><a href="admin?userevent=previouspage">Előző</a></article>
                    <article id="right"><a href="admin?userevent=nextpage">Következő</a></article>
                </section>
            <%}%>
        </div>
    </body>
</html>
