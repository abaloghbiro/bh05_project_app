<%-- 
    Document   : exams
    Created on : 2018.03.16., 17:35:50
    Author     : Dottya
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Vizsgák</title>
    <link href="resources/style.css" rel="stylesheet" type="text/css"/>
  </head>
  <body>
    <div><h2>Vizsgák</h2><p>
        <table class="admintable">
            <tr>
                <td class="admin">Gyakorlat pontszáma</td>
                <td class="admin">Elmélet pontszáma</td>
                <td class="admin">Vizsga dátuma</td>
            </tr>
            <c:forEach items="${exams}" var="exam">
              <tr>
                <td><c:out value="${exam.scorePractice}" /></td>
                <td><c:out value="${exam.scoreTheory}" /></td>
                <td><c:out value="${exam.examDate}" /></td>
              </tr>
            </c:forEach>
        </table>
    </p></div>

    <c:if test="${isIt==true}">
    <h2>Töltsd fel a fájlt .zip formátumban</h2>
    <form action="upload-file" method="post" enctype="multipart/form-data">
      <input type="file" name="uploadedfile" accept=".zip"><br>
      <input type="Submit" value="Upload file"><br>
    </c:if>
    </form>
    <c:if test="${accessLevel==3}">
        <a href="/admin?userevent=showpage"> Admin Oldal </a>
    </c:if>
    <c:if test="${accessLevel>=2}">
        <a href="/instructor"> Tanár Oldal</a>
    </c:if>
        <a href="/ChangePwServlet" > jelszó váltás</a>
  </body>
</html>
