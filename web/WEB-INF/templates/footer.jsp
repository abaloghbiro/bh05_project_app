    <!-- JavaScript files-->
    <script src="resources/vendor/jquery/jquery.min.js"></script>
    <script src="resources/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="resources/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="resources/vendor/chart.js/Chart.min.js"></script>
    <script src="resources/vendor/jquery-validation/jquery.validate.min.js"></script>
    <!-- Main File-->
    <script src="resources/js/front.js"></script>