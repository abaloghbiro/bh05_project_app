<%-- 
    Document   : changePw
    Created on : 2018.03.17., 14:36:52
    Author     : akisreti
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="row">
<div class="col-lg-4"></div>
<div class="col-lg-4">
  <div class="card">
    <div class="card-header d-flex align-items-center">
      <h3 class="h4">Jelszó módosítása</h3>
    </div>
    <div class="card-body">
      <p>Kérem, adja meg új jelszavát.</p>
      <form method="post">
        <div class="form-group">
          <label class="form-control-label">Régi jelszó</label>
          <input type="password" class="form-control">
        </div>
        <div class="form-group">       
          <label class="form-control-label">Új jelszó</label>
          <input type="password" class="form-control">
        </div>
        <div class="form-group">       
          <label class="form-control-label">Új jelszó még egyszer</label>
          <input type="password" class="form-control">
        </div>
        <div class="form-group">       
          <input type="submit" value="Küldés" class="btn btn-primary">
        </div>
      </form>
    </div>
  </div>
</div>
<div class="col-lg-4"></div>
</div>