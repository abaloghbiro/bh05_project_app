<%-- 
    Document   : editUser
    Created on : 2018.03.17., 14:37:12
    Author     : akisreti
--%>

<%@page import="model.daos.GroupDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.daos.BhUserDAO"%>
<%@page import="model.beans.AdminToolsBean"%>
<%@page import="model.beans.UserBean"%>
<jsp:useBean id="admintoolsbean" class="model.beans.AdminToolsBean" scope="session"/>

<div class="row">
<div class="col-lg-4"></div>
<div class="col-lg-4">
  <div class="card">
    <div class="card-close">
      <div class="dropdown">
        <button type="button" id="closeCard1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
        <div aria-labelledby="closeCard1" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
      </div>
    </div>
    <%BhUserDAO user = null;
                                      for (int i = 0; i < admintoolsbean.getUserList().size() && user==null; i++) {
                                        BhUserDAO u = admintoolsbean.getUserList().get(i);
                                        if(u.getId() == admintoolsbean.getSelecteUserIndex()){
                                          user = u; 
                                        }
                                      }
                                    %>
    <div class="card-header d-flex align-items-center">
      <h3 class="h4">Felhasználó módosítása</h3>
    </div>
    <div class="card-body">
      <p></p>
      <form name="edit" action="admin" method="post" form accept-charset="ISO-8859-1">
        <div class="form-group">
          <label class="form-control-label">Felhasználónév</label>
          <input type="email" placeholder="<%=user.getUserName() == null ? "" : user.getUserName() %>"  readonly class="form-control">
        </div>
        <div class="form-group">       
          <label class="form-control-label">Password</label>
          <input type="password" placeholder="Password" class="form-control">
        </div>
        <div class="form-group">       
          <input type="submit" value="Signin" class="btn btn-primary">
        </div>
      </form>
    </div>
  </div>
</div>
<div class="col-lg-4"></div>
</div>
