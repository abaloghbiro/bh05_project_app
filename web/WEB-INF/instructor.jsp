<%@page import="model.dtos.ExamAttendanceDTO"%>
<%@page import="model.SQLUtils"%>
<%@page import="model.dtos.BhGroupDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.dtos.BhUserDTO"%>
<%@page import="model.beans.AdminToolsBean"%>
<%@page import="model.beans.InstructorBean"%>
<%@page import="model.beans.UserBean"%>
<jsp:useBean id="instructorbean" class="model.beans.InstructorBean" scope="session"/>


<%! int access = 1; %>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>BH Portál - Oktató</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="resources/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="resources/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="resources/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="resources/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="resources/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <!-- Data Tables -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <header class="header">
        <nav class="navbar">
          <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
              <!-- Navbar Header-->
              <div class="navbar-header">
                <!-- Navbar Brand --><a href="index.html" class="navbar-brand">
                  <div class="brand-text brand-big"><span>BH </span><strong>Hallgatói portál</strong></div>
                  <div class="brand-text brand-small"><strong>BH</strong></div></a>
                <!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
              </div>
              <!-- Navbar Menu -->
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                <!-- Logout    -->
                <li class="nav-item"><a href="bhservice?userevent=logout" class="nav-link logout">Kijelentkezés<i class="fa fa-sign-out"></i></a></li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
      <div class="page-content d-flex align-items-stretch"> 
        <% int displayPage = ((InstructorBean) session.getAttribute("displayPage")).getPage();%>
        <!-- Side Navbar -->
        <nav class="side-navbar">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <div class="avatar"><img src="resources/img/avatar-1.jpg" alt="..." class="img-fluid rounded-circle"></div>
            <div class="title">
              <h1 class="h4">
             <%=((UserBean) session.getAttribute("userbean")).getName()%></h1>
              <% int access = ((UserBean) session.getAttribute("userbean")).getAccessLevel(); %>
              <p>
                <%if (access == 1) {%>
                Hallgató
                <%}
                else if (access == 2) {%>
                Tanár
                <%}
                else if (access == 3) {%>
                Adminisztrátor
                <%}
                %>
              </p>
            </div>
          </div>
          <!-- Sidebar Navidation Menus--><span class="heading">Menü</span>
          <ul class="list-unstyled">

            <li <% if (displayPage == 1) { %>class="active" <%}%>><a href="instructor?displayPage=1"> <i class="fa fa-file-text"></i>Vizsgák</a></li>
            <li <% if (displayPage == 2) { %>class="active" <%}%>><a href="instructor?displayPage=2"> <i class="fa fa-users"></i>Tanulók</a></li>
            <li <% if (displayPage == 4) { %>class="active" <%}%>><a href="instructor?displayPage=4"> <i class="fa fa-key"></i>Jelszó</a></li>

          </ul>
        </nav>

        <div class="content-inner">
          <!-- Breadcrumb-->
          <% if (displayPage == 1) { %> 
          <section class="tables">   
            <div class="container-fluid">
              <div class="row">


                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Vizsgák</h3>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">                       
                        <table class="table table-striped table-hover" id="userTable">
                          <thead>
                            <tr>
                              <th data-formatter="dateFormat">Vizsga időpontja</th>
                              <th>Vizsga állapota</th>
                              <th>Tanulók száma</th>
                              <th>Funkciók</th>
                            </tr>
                          </thead>
                          <tbody>

                            <tr>
                              <td class="admin">aaa</td>
                              <td class="admin">bbbb</td>
                              <td class="admin">ccc</td>
                              <td class="admin"><a href="instructor?displayPage=3" title="Tanulók"><i class="fa fa-users fa-lg"></i></a>&nbsp;&nbsp;
                                <a href="instructor?displayPage=2"  title="Letöltés"><i class="fa fa-cloud-download fa-lg"></i></a>&nbsp;&nbsp;
                              <a href="instructor?displayPage=2"  title="Fájlok törlése"><i class="fa fa-cloud-download fa-lg"></i></a>
                              </td>
                              <!-- <td class="admin">
                                       <a href="admin?userevent=edit&uid=<//%=user.getId()%>">Szerkesztés</a>
                                   <//%UserBean userbean = (UserBean)session.getAttribute("userbean");
                                     if(!user.getUserName().equals(userbean.getUserName())) { %> 
                                   <//%}%>
                               </td> 
                               <td class="admin">
                                   <//% if(!user.getUserName().equals(userbean.getUserName())) { %>
                                       <a href="admin?userevent=delete&uid=<//%=user.getId()%>">Törlés</a>
                                   <//%}%>
                               </td>-->
                            </tr>
                 
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </section>

          <% }
    else if (displayPage == 2) { %>      
          <section class="tables">   
            <div class="container-fluid">
              <div class="row">


                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Tanulók</h3>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">                       
                        <table class="table table-striped table-hover" id="userTable">
                          <thead>
                            <tr>
                              <th>Vezetéknév</th>
                              <th>Keresztnév</th>
                              <th>Felhasználónév</th>
                              <th>Csoport</th>
                              <th>Jogosultság</th>
                              <th>Funkciók</th>
                            </tr>
                          </thead>
                          <tbody>
                            <%for (int i = 0; i < SQLUtils.getInstance().getBhUserDTOList().size(); i++) {
                                BhUserDTO user = SQLUtils.getInstance().getBhUserDTOList().get(i);%>
                            <tr>
                              <td class="admin"><%=user.getFirstName()%></td>
                              <td class="admin"><%=user.getLastName()%></td>
                              <td class="admin"><%=user.getUserName()%></td>
                              <td class="admin"><%=user.getGroupDTO().getName()%></td>
                              <td class="admin"><%=user.getRightDTO().getAccessLevelName()%></td>
                              <td class="admin">
                                <a href="admin?userevent=edit&uid=<%=user.getId()%>">Szerkesztés</a>
                                <%UserBean userbean = (UserBean) session.getAttribute("userbean");
                                                      if (!user.getUserName().equals(userbean.getUserName())) { %> 
                                <%}%>
                              </td> 
                              <!--<td class="admin">
                                  <//% if(!user.getUserName().equals(userbean.getUserName())) { %>
                                      <a href="admin?userevent=delete&uid=<//%=user.getId()%>">Törlés</a>
                                  <//%}%>
                              </td>-->
                            </tr>
                            <%}%>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </section>
          <% }
         else if (displayPage == 3) {%>

          <section class="tables">   
            <div class="container-fluid">
              <div class="row">


                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Tanulók vizsga szerint</h3>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">                       
                        <table class="table table-striped table-hover" id="userTable">
                          <thead>
                            <tr>
                              <th>Vezetéknév</th>
                              <th>Keresztnév</th>
                              <th>Felhasználónév</th>
                              <th>Csoport</th>
                              <th>Jogosultság</th>
                              <th>Funkciók</th>
                            </tr>
                          </thead>
                          <tbody>
                          
                            <%for (int i = 0; i < SQLUtils.getInstance().getBhUserDTOList().size(); i++) {
                                BhUserDTO user = SQLUtils.getInstance().getBhUserDTOList().get(i);%>
                            <tr>
                              <td class="admin"><%=user.getFirstName()%></td>
                              <td class="admin"><%=user.getLastName()%></td>
                              <td class="admin"><%=user.getUserName()%></td>
                              <td class="admin"><%=user.getGroupDTO().getName()%></td>
                              <td class="admin"><%=user.getRightDTO().getAccessLevelName()%></td>
                              <td class="admin">
                                <a href="admin?userevent=edit&uid=<%=user.getId()%>">Szerkesztés</a>
                                <%UserBean userbean = (UserBean) session.getAttribute("userbean");
                                                      if (!user.getUserName().equals(userbean.getUserName())) { %> 
                                <%}%>
                              </td> 
                              <!--<td class="admin">
                                  <//% if(!user.getUserName().equals(userbean.getUserName())) { %>
                                      <a href="admin?userevent=delete&uid=<//%=user.getId()%>">Törlés</a>
                                  <//%}%>
                              </td>-->
                            </tr>
                            <%}%>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </section>

          <% }
         else if (displayPage == 4) {%>
          <section>
            <%@include file="templates/changePw.jsp" %>
          </section>
          <% }%>
          <!-- Page Footer-->
          <footer class="main-footer">
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-6">
                  <p>BH05 &copy; 2017-2018</p>
                </div>
                <div class="col-sm-6 text-right">
                  <p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a></p>
                  <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="resources/vendor/jquery/jquery.min.js"></script>
    <script src="resources/vendor/popper.js/umd/popper.min.js"></script>
    <script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="resources/vendor/jquery.cookie/jquery.cookie.js"></script>
    <script src="resources/vendor/chart.js/Chart.min.js"></script>
    <script src="resources/vendor/jquery-validation/jquery.validate.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <!-- Main File-->
    <script src="resources/js/front.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script>
$(document).ready(function () {
  $('#userTable').DataTable();
  //$( "input" ).addClass( "form-control" );
}
);


    </script>
  </body>
</html>