<%@page import="model.beans.UserBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>BH Hallhatói portál - Bejelentkezés</title>
    <%@include file="WEB-INF/templates/header.jsp" %>
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-12">
              <div class="info d-flex align-items-center">
                <div class="content">
                      <h1>Nincs jogosultsága az oldal megtekintéséhez! :(</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyrights text-center">
        <p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a>
          <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
        </p>
      </div>
    </div>
    <%@include file="WEB-INF/templates/footer.jsp" %>
  </body>
</html>