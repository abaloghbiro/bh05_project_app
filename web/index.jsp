<%@page import="model.beans.UserBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>BH Hallhatói portál - Bejelentkezés</title>
    <%@include file="WEB-INF/templates/header.jsp" %>
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content">
                  <div class="logo">
                    <h1>Braining Hub</h1>
                  </div>
                  <p>Hallgatói portál</p>
                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content">
                  <form name="login" action="login" id="login-form" method="post">
                    <div class="form-group">
                      <input id="login-username" type="text" name="username" required="" class="input-material">
                      <label for="login-username" class="label-material">Felhasználónév</label>
                    </div>
                    <div class="form-group">
                      <input id="login-password" type="password" name="password" required="" class="input-material">
                      <label for="login-password" class="label-material">Jelszó</label>
                      <input type="hidden" name="userevent" value="login">
                    </div><input class="btn btn-primary" type="submit" id="login" value="Login">
                  </form>
                  <%UserBean userBean = (UserBean) session.getAttribute("userbean");
                  if (userBean != null && userBean.isLoginFailed()) {%>
                  <br>
                  <br>
                  Hibás login név vagy jelszó!
                  <%}%>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyrights text-center">
        <p>Design by <a href="https://bootstrapious.com/admin-templates" class="external">Bootstrapious</a>
          <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
        </p>
      </div>
    </div>
    <%@include file="WEB-INF/templates/footer.jsp" %>
  </body>
</html>