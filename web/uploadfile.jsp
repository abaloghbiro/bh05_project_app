<%-- 
    Document   : uploadfile
    Created on : 03-Mar-2018, 18:29:17
    Author     : Peter
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Upload file</title>
  </head>
  <body>
    <h1>Upload your file in .zip format</h1>
    <form action="upload-file" method="post" enctype="multipart/form-data">
      <input type="radio" name="examID"><br>
      <input type="file" name="uploadedfile" accept=".zip"><br>
      <input type="Submit" value="Upload file"><br>
    </form>
  </body>
</html>
