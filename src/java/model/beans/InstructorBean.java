/* @author Kisréti Ákos
 * @date   2018.03.16.
 */

package model.beans;


public class InstructorBean extends AdminToolsBean{
  
  private int page;

  public InstructorBean() {
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  
}
