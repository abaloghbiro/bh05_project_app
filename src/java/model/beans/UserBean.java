package model.beans;

import java.io.Serializable;
import model.AccessLevelNames;

public class UserBean implements AccessLevelNames, Serializable{

  
  private String userName;
  private String name;
  private int accessLevel;
  private boolean loginFailed;
  

  public UserBean(String userName, String name, int accessLevel) {
    this.userName = userName;
    this.name=name;
    this.accessLevel = accessLevel;
    this.loginFailed = false;
  }

  public int getAccessLevel() {
    return accessLevel;
  }

  public String getUserName() {
    return userName;
  }

  public boolean isLoginFailed() {
    return loginFailed;
  }

  public String getName() {
    return name;
  }
  
  public void setLoginFailed(boolean loginFailed) {
    this.loginFailed = loginFailed;
  }

  @Override
  public String toString() {
    return "UserBean{" + "userName=" + userName + ", name=" + name + ", accessLevel=" + accessLevel + ", loginFailed=" + loginFailed + '}';
  }

  
  
}
