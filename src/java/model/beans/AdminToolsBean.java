package model.beans;

import java.io.Serializable;
import java.util.List;
import model.dtos.BhUserDTO;
import model.dtos.BhGroupDTO;
import model.dtos.RightDTO;

public class AdminToolsBean implements Serializable{
  int selectedUserIndex;
  boolean userEditFailed;
  boolean userCreateFailed;
  boolean userDeleteFailed;
  boolean editUser;
  boolean createUser;
  boolean deleteUser;
  boolean addGroup;
  boolean userEdited;
  boolean userCreated;
  boolean userDeleted;
  String message;
  int firstUserIndex;
  int endUserIndex;
  List<BhUserDTO> userList;
  List<BhGroupDTO> groupList;
  List<RightDTO> rightList;

  public synchronized List<BhUserDTO> getUserList() {
    return userList;
  }

  public synchronized void setUserList(List<BhUserDTO> userList) {
    this.userList = userList;
  }

  public AdminToolsBean() {
  }

  public synchronized int getSelectedUserIndex() {
    return selectedUserIndex;
  }

  public synchronized boolean isUserDeleteFailed() {
    return userDeleteFailed;
  }
  
  public synchronized int getSelecteUserIndex() {
    return selectedUserIndex;
  }

  public synchronized void setSelecteUserIndex(int selecteUserIndex) {
    this.selectedUserIndex = selecteUserIndex;
  }

  public synchronized boolean isUserEditFailed() {
    return userEditFailed;
  }

  public synchronized void setUserEditFailed(boolean userEditFailed) {
    this.userEditFailed = userEditFailed;
  }

  public synchronized boolean isUserCreateFailed() {
    return userCreateFailed;
  }

  public synchronized void setUserCreateFailed(boolean userCreateFailed) {
    this.userCreateFailed = userCreateFailed;
  }

  public synchronized boolean isUserDeleteFailde() {
    return userDeleteFailed;
  }

  public synchronized void setUserDeleteFailed(boolean userDeleteFailde) {
    this.userDeleteFailed = userDeleteFailde;
  }

  public synchronized boolean isEditUser() {
    return editUser;
  }

  public synchronized void setEditUser(boolean editUser) {
    this.editUser = editUser;
  }

  public synchronized boolean isCreateUser() {
    return createUser;
  }

  public synchronized void setCreateUser(boolean createUser) {
    this.createUser = createUser;
  }

  public synchronized boolean isDeleteUser() {
    return deleteUser;
  }

  public synchronized void setDeleteUser(boolean deleteUser) {
    this.deleteUser = deleteUser;
  }

  public synchronized String getUserCreateFailCause() {
    return message;
  }

  public synchronized int getFirstUserIndex() {
    return firstUserIndex;
  }

  public synchronized void setFirstUserIndex(int firstUserIndex) {
    this.firstUserIndex = firstUserIndex;
  }

  public synchronized int getEndUserIndex() {
    return endUserIndex;
  }

  public synchronized void setLastUserIndex(int endUserIndex) {
    this.endUserIndex = endUserIndex;
  }

  public synchronized boolean isUserEdited() {
    return userEdited;
  }

  public synchronized void setUserEdited(boolean userEdited) {
    this.userEdited = userEdited;
  }

  public synchronized boolean isUserCreated() {
    return userCreated;
  }

  public synchronized void setUserCreated(boolean userCreated) {
    this.userCreated = userCreated;
  }

  public synchronized boolean isUserDeleted() {
    return userDeleted;
  }

  public synchronized void setUserDeleted(boolean userDeleted) {
    this.userDeleted = userDeleted;
  }

  public synchronized String getMessage() {
    return message;
  }

  public synchronized void setMessage(String message) {
    this.message = message;
  }

  public synchronized List<BhGroupDTO> getGroupList() {
    return groupList;
  }

  public synchronized void setGroupList(List<BhGroupDTO> groupList) {
    this.groupList = groupList;
  }

  public synchronized List<RightDTO> getRightList() {
    return rightList;
  }

  public synchronized void setRightList(List<RightDTO> rightList) {
    this.rightList = rightList;
  }

  public synchronized boolean isAddGroup() {
    return addGroup;
  }

  public synchronized void setAddGroup(boolean addGroup) {
    this.addGroup = addGroup;
  }

  @Override
  public synchronized String toString() {
    return "AdminToolsBean{" + "selectedUserIndex=" + selectedUserIndex + ", userEditFailed=" + userEditFailed + ", userCreateFailed=" + userCreateFailed + ", userDeleteFailed=" + userDeleteFailed + ", editUser=" + editUser + ", createUser=" + createUser + ", deleteUser=" + deleteUser + ", userEdited=" + userEdited + ", userCreated=" + userCreated + ", userDeleted=" + userDeleted + ", message=" + message + ", firstUserIndex=" + firstUserIndex + ", endUserIndex=" + endUserIndex + ", userList=" + userList + ", groupList=" + groupList + ", rightList=" + rightList + '}';
  }
  
  
  
}
