package model;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import model.dtos.BhUserDTO;
import model.entities.BhGroup;
import model.entities.BhUser;
import model.entities.Exam;
import model.entities.ExamFile;
import model.entities.ExamAttendance;
import model.entities.Right;
import model.entities.Session;

public interface ModelInterface {
  
  void setEntityFactoryManager(EntityManagerFactory emf);
  
  boolean insertExamFile(Date uploadTime, String path, BhUser user, Exam examDAte);
  
  boolean insertUser(BhUser user);
  
  boolean insertExamDate(Date startDate, Date endDate, int maxScoreTheory, int maxScorePractice);
  
  boolean insertExamDate(Exam examDate);
  
  boolean insertGroup(String groupName);
  
  boolean insertExamResult(ExamAttendance result);
  
  boolean insertSession(Session session);
  
  boolean deleteExamFile(int examID);
   
  boolean updateUserData(BhUser user);

  boolean updatePassword(int userID, String newPassword);
    
  List<ExamFile> getExamFiles(Exam examDate);
  
  BhUser getUserByID(int userID);
  
  BhUserDTO getUserByUserName(String userName);
  
  Exam getExamDateByID(int examID);
  
  Exam getExamDateByStartDate(Date date);
  
  ExamFile getExamFileByID(int fileID);
  
  ExamAttendance getExamResultByID(int examResultID);
  
  BhGroup getBHGroupByID(int groupID);
  
  Right getRightByID(int rightID);
  
  Session getSessionByID(int sessionID);
}
