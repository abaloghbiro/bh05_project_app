package model;

import java.sql.Timestamp;
import java.util.Date;


public class ModelUtility {
  public static Timestamp DateToTimestamp(Date date){
    return null == date ?  null : new Timestamp(date.getTime());
  }
}
