package model;


import model.entities.ExamAttendance;
import model.entities.Right;
import model.entities.Session;
import model.entities.BhGroup;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.List;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import model.dtos.BhUserDTO;
import model.dtos.ExamAttendanceDTO;
import model.dtos.BhGroupDTO;
import model.dtos.RightDTO;
import model.entities.BhUser;
import model.entities.Exam;
import model.entities.ExamFile;

public class SQLUtils {
  private static SQLUtils sqlUtilsInstance;
  private EntityManagerFactory emf;

  private SQLUtils() {
    emf = Persistence.createEntityManagerFactory("toldacuccot2PU");
  }

  public static SQLUtils getInstance(){
    if(SQLUtils.sqlUtilsInstance == null ){
      sqlUtilsInstance = new SQLUtils();
    }
    return sqlUtilsInstance;
  }

  //tesztelve
  public boolean insertUser(BhUser user) {
    EntityManager em = emf.createEntityManager();
    try {
      em.getTransaction().begin();
      em.persist(user);
      em.getTransaction().commit();
      em.close();
    }
    catch (PersistenceException ex) {
      return false;
    }
    return true;
  }
  
  public boolean insertUser(String userName, String firstName, String lastName, String password, 
          int active, String groupName, String rightName){
    boolean returnValue = false;
    EntityManager em = emf.createEntityManager();
    try {
      TypedQuery<BhGroup> gtq = em.createQuery("SELECT g FROM BhGroup g WHERE UPPER(g.name) = '" + groupName.toUpperCase() + "'", BhGroup.class);
      BhGroup bhGroup = gtq.getSingleResult();
      TypedQuery<Right> rtq = em.createQuery("SELECT r FROM Right r WHERE UPPER(r.accessLevelName) = '" + rightName.toUpperCase() + "'", Right.class);
      Right right = rtq.getSingleResult();
      BhUser bhUser = new BhUser(userName, firstName, lastName, password, active, bhGroup, right);
      bhUser.setRight(right);
      em.getTransaction().begin();
      em.persist(bhUser);
      em.getTransaction().commit();
      em.close();
      returnValue = true;
    } 
    catch (Exception e) {
      return false;
    }
    return returnValue;
  }
  
  public boolean updateUser(int userId, String firstName, String lastName, String password, String group, String rightName, int active) {
    boolean returnValue = false;
    EntityManager em = emf.createEntityManager();
    try {
      BhUser bhUser = em.find(BhUser.class, userId);
      bhUser.setLastName(lastName.toUpperCase());
      bhUser.setFirstName(firstName.toUpperCase());
      if(!password.isEmpty())
        bhUser.setBhPass(password);
      bhUser.setActive(active);
      TypedQuery<BhGroup> gtq = em.createQuery("SELECT g FROM BhGroup g WHERE UPPER(g.name) = '" + group.toUpperCase() + "'", BhGroup.class);
      BhGroup bhGroup = gtq.getSingleResult();
      bhUser.setGroup(bhGroup);
      TypedQuery<Right> rtq = em.createQuery("SELECT r FROM Right r WHERE UPPER(r.accessLevelName) = '" + rightName.toUpperCase() + "'", Right.class);
      Right right = rtq.getSingleResult();
      bhUser.setRight(right);
      em.getTransaction().begin();
      em.persist(bhUser);
      em.getTransaction().commit();
      em.close();
      returnValue = true;
    } catch (Exception e) {
      return false;
    }
    return returnValue;
  }
  
  // tesztelve
  public boolean insertExamFile(Date uploadTime, String path, BhUser user, Exam examDate) {
    EntityManager em = emf.createEntityManager();
    ExamFile examFile = new ExamFile(uploadTime, path, user, examDate);
    try {
      em.getTransaction().begin();
      em.persist(examFile);
      em.getTransaction().commit();
      em.close();
    } catch (PersistenceException ex) {
      return false;
    }
    return true;
  }
  
  //tesztelve
  public boolean insertExamDate(Exam examDate){
    EntityManager em = emf.createEntityManager();
    try {
      em.getTransaction().begin();
      em.persist(examDate);
      em.getTransaction().commit();
      em.close();
    } catch (PersistenceException ex) {
      return false;
    }
    return true;
  }

  //tesztelve
  public boolean insertExamResult(ExamAttendance result) {
    EntityManager em = emf.createEntityManager();
    try {
      em.getTransaction().begin();
      em.persist(result);
      em.getTransaction().commit();
      em.close();
    } catch (PersistenceException ex) {
      return false;
    }
    return true;
  }

  public boolean updateUserData(BhUser updatedUser) {
    EntityManager em = emf.createEntityManager();
    try{
      em.getTransaction().begin();
      em.merge(updatedUser);
      em.getTransaction().commit();
      em.close();
    }
    catch (PersistenceException ex) {
      System.out.println(ex.getMessage());
      return false;
    }
    return true;
  }

  public boolean updatePassword(int userID, String newPassword) {
//    hu.bh.mv.BCrypt.hashpw(newPassword, hashSalt);
//    EntityManager em = emf.createEntityManager();
//    BhUser user = em.find(BhUser.class, userID);
//    if(null!=user){
//      user.setBhPass(newPassword);
//      em.getTransaction().begin();
//      em.persist(user);
//      em.getTransaction().commit();
//      em.close();
//    }
//    else
//      return false;
    return true;
  }

	
  public boolean deleteExamFile(int examID) {
    EntityManager em = emf.createEntityManager();
    ExamFile file = null;
    try {
      file = em.find(ExamFile.class, examID);
      if (null == file){
        return false;
      }
//      em.flush();
      System.out.println(file);
      
      em.getTransaction().begin();
      em.remove(file);
      em.getTransaction().commit();
      em.close();
    }
    catch (PersistenceException e) {
      System.out.println(e.getMessage());
      return false;
    }
    return true;
  }

  //tesztelve
//  @Override
//  public List<ExamFile> getExamFiles(ExamDate examDate) {
//		List<ExamFile> examFilesList = new ArrayList<>();
//    EntityManager em = emf.createEntityManager();
//    try {
//    CriteriaBuilder builder = em.getCriteriaBuilder();
//    CriteriaQuery<ExamFile> criteria = builder.createQuery(ExamFile.class);
//    Root<ExamFile> from = criteria.from(ExamFile.class);
//    criteria.select(from);
//    criteria.where(builder.equal(from.get(ExamFile_.examId), examDate));
//    TypedQuery<ExamFile> query = em.createQuery(criteria);
//		examFilesList = query.getResultList();
//      return examFilesList;
//    } catch (NoResultException e) {
//      return null;
//    }
//  }

  //tesztelve
  public boolean insertExam(Date examDate,  String examTitle, int examState, int maxScoreTheory, int maxScorePractice) {
    EntityManager em = emf.createEntityManager();
    Exam exam = new Exam(examDate, examTitle, examState, new BigDecimal(maxScoreTheory), new BigDecimal(maxScoreTheory));
    try {
      em.getTransaction().begin();
      em.persist(exam);
      em.getTransaction().commit();
      em.close();
    } catch (PersistenceException ex) {
      return false;
    }
    return true;
  }

	public BhGroup getBHGroupByID(int groupID) {
		BhGroup group = null;
	try{
		EntityManager em = emf.createEntityManager();
		group = em.find(BhGroup.class, groupID);
	}catch(IllegalStateException | IllegalArgumentException ex){
		
	}
	return group;
	}

	public Right getRightByID(int rightID) {
		Right right = null;
		try {
			EntityManager em = emf.createEntityManager();
			right = em.find(Right.class, rightID);
		} catch (Exception e) {
		}
		return right;
	}

  public ArrayList<RightDTO> getRightDTOList(){
    ArrayList<RightDTO> list = new ArrayList<>();
    EntityManager em = emf.createEntityManager();
    TypedQuery<Right> tq = em.createNamedQuery("Right.findAll", Right.class);
    List<Right> rl = tq.getResultList();
    if(rl != null){
      for (Right right : rl) {
        list.add(new RightDTO(right.getId(), right.getAccessLevel(), right.getAccessLevelName()));
      }
    }
    return list;
  }
  
  //tesztelve
  public boolean insertGroup(String groupName) {
    EntityManager em = emf.createEntityManager();
    BhGroup group = new BhGroup();
    group.setName(groupName);
    try {
      em.getTransaction().begin();
      em.persist(group);
      em.getTransaction().commit();
      em.close();
    }
    catch (PersistenceException e) {
      return false;
    }
    return true;
  }

  public List<BhGroupDTO> getGroupDTOList(){
    ArrayList<BhGroupDTO> list = new ArrayList<>();
    EntityManager em = emf.createEntityManager();
    TypedQuery<BhGroup> tq = em.createNamedQuery("BhGroup.findAll", BhGroup.class);
    List<BhGroup> rl = tq.getResultList();
    if(rl != null){
      for (BhGroup group : rl) {
        list.add(new BhGroupDTO(group.getId(), group.getName()));
      }
    }
    return list;
  }
  
  public boolean insertSession(Session session) {
    EntityManager em = emf.createEntityManager();
    try {
      em.getTransaction().begin();
      em.persist(session);
      em.getTransaction().commit();
      em.close();
    }
    catch (PersistenceException e) {
      return false;
    }
    return true;
  }
  
  public BhUserDTO getUserByUserName(String userName){
		BhUserDTO bhUserDTO=null;
		EntityManager em=null;
		try{
    em = emf.createEntityManager();
		TypedQuery q =  em.createQuery("SELECT u FROM BhUser u where u.userName = '"+userName+"'",BhUser.class);
//    BhUser user = em.find(BhUser.class, userName);
		BhUser user = (BhUser) q.getSingleResult();
    bhUserDTO = new BhUserDTO(user.getId(), user.getUsername(), user.getFirstName(),
              user.getLastName(), user.getBhPass(), user.getActive(),
              user.getGroup() ,user.getRight());
		}catch(Exception ex) {
//			bhUserDAO=null;
		}finally{
    if(null!=em)
			try {
			em.close();
			} catch (Exception e) {
			}
		}
    return bhUserDTO;
  }

	public List<ExamAttendanceDTO> getExamResults(String userName){
		List<ExamAttendanceDTO> examAttandanceDTOList = new ArrayList<>();
		List<ExamAttendance> entityList = new ArrayList<>();
		int id = getUserByUserName(userName).getId();
		Float practice = -1f;
		Float theory = -1f;
		EntityManager em = null;
		try{
			em=emf.createEntityManager();
			TypedQuery q = em.createQuery("SELECT e FROM ExamAttendance e", ExamAttendance.class); //valamiért nem működik a lekérdezés, de amúgy jó a metódus.. (by Timót) asszem megszereltem..
			entityList = q.getResultList();
			for (int i = 0; i < entityList.size(); i++) {
				if(entityList.get(i).getUser().getId()==id){
					if(null==entityList.get(i).getScorePractice())
						practice = null;
					else
						practice = entityList.get(i).getScorePractice().floatValue();
					if(null==entityList.get(i).getScoreTheory())
						theory=null;
					else
						theory=entityList.get(i).getScoreTheory().floatValue();
					
					examAttandanceDTOList.add(new ExamAttendanceDTO(practice, theory, entityList.get(i).getExam().getExamDate(), entityList.get(i).getUser().getUsername()));
			}
			}
		} catch (Exception ex) {
		}
		return examAttandanceDTOList;
	}
//	
  //tesztelve
//  @Override
//  public ExamDate getExamDateByStartDate(final Date date) {
//    if (null == date) {
//      return null;
//    }
//    EntityManager em = emf.createEntityManager();
//    Timestamp timestamp = ModelUtility.DateToTimestamp(date);
//
//    CriteriaBuilder builder = em.getCriteriaBuilder();
//    CriteriaQuery<ExamDate> criteria = builder.createQuery(ExamDate.class);
//    Root<ExamDate> from = criteria.from(ExamDate.class);
//    criteria.select(from);
//    criteria.where(builder.equal(from.get(ExamDate_.dtStart), timestamp.getTime()));
//
//    TypedQuery<ExamDate> query = em.createQuery(criteria);
//    try {
//      return query.getSingleResult();
//    } catch (NoResultException e) {
//      return null;
//    }
//  }

	public int getBHGroupID(String groupName){
		int id = -1;
		try{
		EntityManager em = emf.createEntityManager();
		id = em.find(BhGroup.class, groupName).getId();
		em.close();
		}catch(IllegalArgumentException | IllegalStateException ex){
			
		}
		return id;
	}
	
	public int getRightID(Integer accessLevel){
		int id=0;
		try{
		EntityManager em = emf.createEntityManager();
		id = em.find(Right.class, accessLevel).getId();
		em.close();
		}catch(IllegalArgumentException | IllegalStateException ex){
			
		}
		return id;
	}
	
	public List<BhUserDTO> getBhUserDTOList(){
		EntityManager em =  emf.createEntityManager();
		List<BhUserDTO> listDTO = new ArrayList<>();
		List<BhUser> listEntity;
		try{
		TypedQuery q = em.createQuery("SELECT u from BhUser u", BhUser.class);
		listEntity = q.getResultList();
		for (BhUser bhUser : listEntity) {
			listDTO.add(new BhUserDTO(bhUser.getId(), bhUser.getUsername(), bhUser.getFirstName(), 
              bhUser.getLastName(), bhUser.getBhPass(), bhUser.getActive(), bhUser.getGroup(), bhUser.getRight()));		
    }
		em.close();
		} catch (IllegalArgumentException ex ){
			
		}
		return listDTO;
	}

	public List<Integer> getExamIdByExamDate (Date examDate){
		List<Exam> examList = new ArrayList<>();
		List<Integer> idList = new ArrayList<>();
		try {
			EntityManager em = emf.createEntityManager();
			TypedQuery q =  em.createQuery("SELECT e FROM Exam e where e.examDate = "+examDate ,Exam.class);
			examList = q.getResultList();
			for (Exam exam : examList) {
				idList.add(exam.getId());
			}
			em.close();
		} catch (Exception e) {
		}
		return idList;
	}

	public boolean isItExamTime(Date examDate){
		List<Exam> examList = new ArrayList<>();
		boolean isItExamTime = false;
		try {
			EntityManager em = emf.createEntityManager();
			TypedQuery q =  em.createQuery("SELECT e FROM Exam e where e.examDate = "+examDate ,Exam.class);
			examList = q.getResultList();
			} catch (Exception e) {
		}
		for (Exam exam : examList) {
			if(exam.getExamState() == 1 && exam.getExamDate().after(new Date()))
				isItExamTime=true;
		}
		return isItExamTime;
	}
	
	public BhUser getUserByID(int userID) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	public Exam getExamDateByID(int examID) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	public Exam getExamDateByStartDate(Date date) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	public ExamFile getExamFileByID(int fileID) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	public ExamAttendance getExamResultByID(int examResultID) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	public Session getSessionByID(int sessionID) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	public List<ExamFile> getExamFiles(Exam examDate) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
  
  public List<ExamAttendanceDTO> getExams (){
		List<Exam> examList = new ArrayList<>();
		List<ExamAttendanceDTO> ExamDTOList = new ArrayList<>();
		try {
			EntityManager em = emf.createEntityManager();
			examList =  em.createNamedQuery("Exam.findAll").getResultList();
			for (Exam exam : examList) {
				/*ExamDTOList.add(new ExamAttendanceDTO(exam.getId(), exam.getExamDate(), exam.getExamState(),
                exam.getMaxScoreTheory(), exam.getMaxScorePractice(), exam.getExamResultList()));*/
			}
			em.close();
		} catch (Exception e) {
		}
		return ExamDTOList;
	}
  
}