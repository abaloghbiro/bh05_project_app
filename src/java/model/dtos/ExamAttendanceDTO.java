package model.dtos;

import java.io.Serializable;
import java.util.Date;

public class ExamAttendanceDTO implements Serializable{
	private Float scorePractice;
	private Float scoreTheory;
	private Date examDate;
	private String userName;

	public ExamAttendanceDTO(Float scorePractice, Float scoreTheory, Date examDate, String userName) {
		this.scorePractice = scorePractice;
		this.scoreTheory = scoreTheory;
		this.examDate = examDate;
		this.userName = userName;
	}

	public Float getScorePractice() {
		return scorePractice;
	}

	public Float getScoreTheory() {
		return scoreTheory;
	}

	public Date getExamDate() {
		return examDate;
	}

	public String getUserName() {
		return userName;
	}
	
}
