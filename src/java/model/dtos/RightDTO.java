package model.dtos;

import java.io.Serializable;
import java.util.List;

public class RightDTO implements Serializable{
  private Integer id;
  private Integer accessLevel;  
  private String accessLevelName;
  private List<BhUserDTO> bhUserList;

  public RightDTO(Integer id, Long accessLevel, String accessLevelName) {
    this.id = id;
    this.accessLevel = (int)(long)accessLevel;
    this.accessLevelName = accessLevelName;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getAccessLevel() {
    return accessLevel;
  }

  public void setAccessLevel(Integer accessLevel) {
    this.accessLevel = accessLevel;
  }

  public String getAccessLevelName() {
    return accessLevelName;
  }

  public void setAccessLevelName(String accessLevelName) {
    this.accessLevelName = accessLevelName;
  }

  public List<BhUserDTO> getBhUserList() {
    return bhUserList;
  }

  public void setBhUserList(List<BhUserDTO> bhUserList) {
    this.bhUserList = bhUserList;
  }
  
}
