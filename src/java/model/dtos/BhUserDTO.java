package model.dtos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import model.entities.BhGroup;
import model.entities.ExamAttendance;
import model.entities.Right;

public class BhUserDTO implements Serializable{
  private int id;
  private String userName;
  private String firstName;
  private String lastName;
  private String bhPass;
  private int active;
  private List<ExamAttendanceDTO> examAttendanceDTOList = new ArrayList<>();
  private BhGroupDTO group;
  private RightDTO right;

  public BhUserDTO(int id, String userName, String firstName, String lastName, String bhPass, int active, /*List<ExamAttendance> examAttendanceList*/
						 BhGroup group, Right right) {
    this.id = id;
    this.userName = userName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.bhPass = bhPass;
    this.active = active;
//		for (ExamAttendance examAttendance : examAttendanceList) {
//			ExamAttendanceDTO dto = new ExamAttendanceDTO(examAttendance.getScorePractice().floatValue(), examAttendance.getScoreTheory().floatValue(), 
//							examAttendance.getExam().getExamDate(), examAttendance.getUser().getUsername());
//		this.examAttendanceDTOList.add(dto);
//		}
//    this.sessionList = sessionList;
//    this.examFileList = examFileList;
    this.group =  new BhGroupDTO(group.getId(), group.getName());
    this.right = new RightDTO(right.getId(), right.getAccessLevel(), right.getAccessLevelName());
  }
  
  public int getId() {
    return id;
  }

  public String getUserName() {
    return userName;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getBhPass() {
    return bhPass;
  }

  public int getActive() {
    return active;
  }

	public List<ExamAttendanceDTO> getExamAttendanceList() {
		return examAttendanceDTOList;
	}

	public void setExamAttendanceList(List<ExamAttendanceDTO> examAttendanceList) {
		this.examAttendanceDTOList = examAttendanceList;
	}

	

//  public List<ExamResult> getExamResultList() {
//    return examResultList;
//  }
//
//  public List<Session> getSessionList() {
//    return sessionList;
//  }
//
//  public List<ExamFile> getExamFileList() {
//    return examFileList;
//  }

  public BhGroupDTO getGroupDTO() {
    return group;
  }

  public RightDTO getRightDTO() {
    return right;
  }

  public void setId(int id) {
    this.id = id;
  }
	
//	private List<ExamAttendanceDTO> buildExamAttendenceList(List<ExamAttendance> examAttendenceList){
//		List<ExamAttendanceDTO> dtoList = new ArrayList<>();
//		for (ExamAttendance examAttendance : examAttendenceList) {
//			dtoList.add(new ExamAttendanceDTO(examAttendance.getScorePractice().floatValue(), examAttendance.getScoreTheory().floatValue(), 
//							examAttendance.getExam().getExamDate(), examAttendance.getUser().getUsername()));	
//		}
//		return dtoList;
//	}
  
}
