package model;

public interface SQLInterface {
  
  String DRIVER="oracle.jdbc.driver.OracleDriver";  
  String URL="jdbc:oracle:thin:@localhost:1521:xe";
  String USER="project6";
  String PASSWORD="project6";

  String SQL_INSERT_FILE=
    "INSERT INTO FILES (ID, UPLOAD_TIME, PATH, EXAM_ID, USER_ID)" + 
    "VALUES (ID_SEQ.NEXTVAL, SYSDATE, ?, ?, ?)";
}
