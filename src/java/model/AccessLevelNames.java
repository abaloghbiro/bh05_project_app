package model;

public interface AccessLevelNames{
  public static final int ACCESS_LEVEL_NONE = 0;
  public static final int ACCESS_LEVEL_STUDENT = 1;
  public static final int ACCESS_LEVEL_INSTRUCTOR = 2;
  public static final int ACCESS_LEVEL_ADMIN = 3;
  public static final String[] ACCESS_LEVEL_NAMES = new String[] {"NONE", "STUDENT", "INSTRUCTOR", "ADMIN"};
}
