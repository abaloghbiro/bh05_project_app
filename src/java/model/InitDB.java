package model;

import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import model.entities.BhUser;
import model.entities.Exam;
import model.entities.Right;
import model.entities.BhGroup;
import model.entities.ExamAttendance;

public class InitDB {

  final static String[] GROUPS = {"staff", "BH05", "BH06"};
  final static String[][] USERS = {
    {"pkiss", "Kiss", "Péter", "pkiss", "staff", "Admin"},
    {"anagy", "Nagy", "András", "anagy", "BH05", "Student"},
    {"ahorvath", "Horváth", "Aladár", "ahorvath", "staff", "Instructor"},
    {"nnorveg","Norvég", "Nóra", "nnorveg", "BH05", "Student"},
    {"vvegh","Végh", "Vaszilij", "vvegh", "BH06", "Student"},
    {"ookos", "Okos", "Ottó", "ookos", "BH06", "Student"},
    {"ppore","Pőre", "Pál", "ppore", "BH05", "Student"}
    };
  static final long [] EXAM_DATES = {520099602639L, 1521099102639L, 1522099122639L};
  static final String [] EXAM_TITLES = {"ALAPOZÓ", "OOP", "ZÁRÓ"};
 
  
  private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("toldacuccot2PU");

  private static boolean initRights() {
    EntityManager em = emf.createEntityManager();
    em.getTransaction().begin();
    for (int i = 0; i < Right.ACCESS_LEVEL_NAMES.length; i++) {
      Right right = new Right(null, i);
      em.persist(right);
    }
    em.getTransaction().commit();
    em.close();
    return true;
  }
  
  private static boolean initGroups() {
    boolean ok = true;
    //EntityManager em = emf.createEntityManager();
    //em.createNativeQuery("CREATE ")
    for (String group : GROUPS) {
      ok=SQLUtils.getInstance().insertGroup(group.toUpperCase());
    }
    return ok;
  }
  
  private static boolean initUsers() {
    boolean ok = false;
    BhUser user = null;
    EntityManager em = emf.createEntityManager();

    for (String[] userData : USERS) {
      Query q = em.createQuery("SELECT r FROM Right r WHERE r.accessLevelName = '" + userData[5].toUpperCase() + "'");
      Right right = null;
      try {
        right = (Right) q.getSingleResult();        
      } catch (Exception e) {
      }

      q = em.createQuery("SELECT g FROM BhGroup g WHERE g.name = '" + userData[4].toUpperCase() + "'");
      BhGroup group = null;
      try {
        group = (BhGroup) q.getSingleResult(); 
      } catch (Exception e) {
      }
      if(right != null && group != null){
        em.getTransaction().begin();
        user = new BhUser(userData[0].toUpperCase(), userData[1].toUpperCase(), userData[2].toUpperCase(), 
              hu.bh.mv.BCrypt.hashpw(userData[3],hu.bh.mv.BCrypt.gensalt(8)), 1, 
              group, right);
        em.persist(user);
        em.getTransaction().commit();
      }
    }
    em.close();
    return true;
  }
  
  private static boolean initExams() {
    boolean ok = false;
    Exam exam = null;
    EntityManager em = emf.createEntityManager();
    for (int i = 0; i< EXAM_DATES.length; i++) {
      exam = new Exam();
      exam.setExamDate(new Date(EXAM_DATES[i]));
      exam.setExamTitle(EXAM_TITLES[i]);
      exam.setExamState(i%2);
      em.getTransaction().begin();
      em.persist(exam);
      em.getTransaction().commit();
    }
    em.close();
    return true;
  }
  
  private static boolean initExamAttendances(){
    boolean result = true;
    EntityManager em = emf.createEntityManager();
    for (int i = 0; i < USERS.length; i++) {
      try{
      ExamAttendance examAttendance = new ExamAttendance(); 
      TypedQuery<Exam> tqExam = em.createQuery(
              "SELECT ex FROM Exam ex WHERE ex.examTitle = '" + EXAM_TITLES [i/3]+"'", Exam.class);
      Exam exam = tqExam.getSingleResult();
      examAttendance.setExam(exam);
      TypedQuery<BhUser> tqBhUser = em.createQuery(
              "SELECT u FROM BhUser u WHERE u.userName = UPPER('" + USERS[i][0] +"')", BhUser.class);
      BhUser user = tqBhUser.getSingleResult();
      examAttendance.setUser(user);
      em.getTransaction().begin();
      em.persist(examAttendance);
      em.getTransaction().commit();
      }
      catch (Exception ex){
        result = false;
      }
    }
    em.close();
    return result;
  }
  
  
  public static boolean initToldacuccot2DB() {
    boolean ok = true;
    ok = initGroups();
    System.out.println("InitGroups: " + ok);
    ok = initRights();
    System.out.println("InitRights: " + ok);
    ok = initUsers();
    System.out.println("InitUsers: " + ok);
    ok = initExams();
    System.out.println("InitExam: " + ok);
    ok = initExamAttendances();
    System.out.println("InitExamAttendance: " + ok);
//    EntityManager em = emf.createEntityManager();
//    Query q = em.createQuery("SELECT u FROM BhUser u ORDER BY ");
    
    
    
    
    return ok;
  }

  
 
  public static void main(String[] args) {
    //SQLUtils.getInstance().setEntityFactoryManager(emf);
    System.out.println(emf.getProperties());
    initToldacuccot2DB();
//    System.out.println(MODEL.getRightByID(1));
//    
//    ExamDate examDate = MODEL.getExamDateByID(1);
//    System.out.println("");
//    System.out.println(examDate);
//    System.out.println("");
//     
//    BhUser user = MODEL.getUserByID(10);
//    System.out.println(user);
//    System.out.println("");
//    
//    user.setFirstName("Módosított Förszt Ném");
//    System.out.println(MODEL.updateUserData(user));
//    System.out.println(MODEL.deleteExamFile(2));
    
//    MODEL.insertUser(new BhUser("Újabb", "Emberke", "Jelszó", MODEL.getBHGroupByID(2), MODEL.getRightByID(2)));
    
//    MODEL.deleteUser(10);
    
//    MODEL.insertExamFile(new Date(), "./files/zh23.zip", user, examDate);
//    MODEL.insertExamDate(new Date(1522100122639L), new Date(1522100122639L), 156, 150);
//    MODEL.insertGroup("tesztCsapat1");
//
//    ExamResult result1 = new ExamResult(new BigDecimal(160), new BigDecimal(140), user, examDate);
//    ExamResult result2 = new ExamResult(new BigDecimal(170), new BigDecimal(170), user, examDate);
//    MODEL.insertExamResult(result1);
//    MODEL.insertExamResult(result2);
    
//    List<ExamResult> results = new ArrayList<ExamResult>();
//    results.add(result1);
//    results.add(result2);
//    user.setExamResultList(results);
//    user.setId(1);
//    System.out.println(user.getId());
//    MODEL.insertUser(user);
    
//    System.out.println(MODEL.getExamDateByStartDate(new Date(1520002639L)));
//    List<ExamFile> files = MODEL.getExamFiles(examDate);
//    for (ExamFile file : files) {
//      System.out.println(file);
//    }
  }
}
