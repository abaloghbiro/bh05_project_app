/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Lini
 */
@Entity
@Table(name = "EXAMS")
@NamedQueries({
  @NamedQuery(name = "Exam.findAll", query = "SELECT e FROM Exam e")
  , @NamedQuery(name = "Exam.findById", query = "SELECT e FROM Exam e WHERE e.id = :id")
  , @NamedQuery(name = "Exam.findByDtStart", query = "SELECT e FROM Exam e WHERE e.examDate = :examDate")
  , @NamedQuery(name = "Exam.findByMaxScoreTheory", query = "SELECT e FROM Exam e WHERE e.maxScoreTheory = :maxScoreTheory")
  , @NamedQuery(name = "Exam.findByMaxScorePractice", query = "SELECT e FROM Exam e WHERE e.maxScorePractice = :maxScorePractice")})
public class Exam implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @Basic(optional = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EXAM_SEQ")
  @Column(name = "ID")
  private Integer id;
  @Basic(optional = false)
  @Column(name = "EXAM_DATE", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date examDate;
  @Basic(optional = false)
  @Column(name = "EXAM_TITLE", nullable = false)
  private String examTitle;  
  @Basic(optional = false)
  @Column(name = "EXAM_STATE", nullable = false)
  private Integer examState;
  // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
  @Column(name = "MAX_SCORE_THEORY", nullable = true)
  private BigDecimal maxScoreTheory;
  @Column(name = "MAX_SCORE_PRACTICE", nullable = true)
  private BigDecimal maxScorePractice;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "exam")
  private List<ExamAttendance> examResultList;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "exam")
  private List<ExamFile> examFileList;

  public Exam() {
  }

  public Exam(Integer id) {
    this.id = id;
  }

  public Exam(Integer id, Date examDate) {
    this.id = id;
    this.examDate = examDate;
  }

  public Exam(Date examDate, String examTitle, Integer examState, BigDecimal maxScoreTheory, BigDecimal maxScorePractice) {
    this.examDate = examDate;
    this.maxScoreTheory = maxScoreTheory;
    this.maxScorePractice = maxScorePractice;
    this.examState = examState;
  }  
  
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Date getExamDate() {
    return examDate;
  }

  public void setExamDate(Date dtStart) {
    this.examDate = dtStart;
  }

  public String getExamTitle() {
    return examTitle;
  }

  public void setExamTitle(String examTitle) {
    this.examTitle = examTitle;
  }

  public Integer getExamState() {
    return examState;
  }

  public void setExamState(Integer state) {
    this.examState = state;
  }
  
  public BigDecimal getMaxScoreTheory() {
    return maxScoreTheory;
  }

  public void setMaxScoreTheory(BigDecimal maxScoreTheory) {
    this.maxScoreTheory = maxScoreTheory;
  }

  public BigDecimal getMaxScorePractice() {
    return maxScorePractice;
  }

  public void setMaxScorePractice(BigDecimal maxScorePractice) {
    this.maxScorePractice = maxScorePractice;
  }

  public List<ExamAttendance> getExamResultList() {
    return examResultList;
  }

  public void setExamResultList(List<ExamAttendance> examResultList) {
    this.examResultList = examResultList;
  }

  public List<ExamFile> getExamFileList() {
    return examFileList;
  }

  public void setExamFileList(List<ExamFile> examFileList) {
    this.examFileList = examFileList;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Exam)) {
      return false;
    }
    Exam other = (Exam) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "model.entities.ExamDate[ id=" + id + " ]";
  }
  
}
