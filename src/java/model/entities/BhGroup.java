/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Lini
 */
@Entity
@Table(name = "GROUPS")
@NamedQueries({
  @NamedQuery(name = "BhGroup.findAll", query = "SELECT b FROM BhGroup b")
  , @NamedQuery(name = "BhGroup.findById", query = "SELECT b FROM BhGroup b WHERE b.id = :id")
  , @NamedQuery(name = "BhGroup.findByName", query = "SELECT b FROM BhGroup b WHERE b.name = :name")})
public class BhGroup implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GROUPS_SEQ")
  @Basic(optional = false)
  @Column(name = "ID")
  private Integer id;
  @Basic(optional = false)
  @Column(name = "NAME", nullable = false, unique = true)
  private String name;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "group")
  private List<BhUser> bhUserList;

  public BhGroup() {
    
  }

  public BhGroup(Integer id) {
    this.id = id;
  }

  public BhGroup(Integer id, String name) {
    this.id = id;
    this.name = name;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<BhUser> getBhUserList() {
    return bhUserList;
  }

  public void setBhUserList(List<BhUser> bhUserList) {
    this.bhUserList = bhUserList;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof BhGroup)) {
      return false;
    }
    BhGroup other = (BhGroup) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "model.entities.BHGroup[ id=" + id + " ]";
  }
  
}
