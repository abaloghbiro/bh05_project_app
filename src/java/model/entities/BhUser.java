package model.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "BH_USER")
@NamedQueries({
  @NamedQuery(name = "BhUser.findAll", query = "SELECT b FROM BhUser b")
  , @NamedQuery(name = "BhUser.findById", query = "SELECT b FROM BhUser b WHERE b.id = :id")
  , @NamedQuery(name = "BhUser.findByFirstName", query = "SELECT b FROM BhUser b WHERE b.firstName = :firstName")
  , @NamedQuery(name = "BhUser.findByLastName", query = "SELECT b FROM BhUser b WHERE b.lastName = :lastName")
  , @NamedQuery(name = "BhUser.findByBhPass", query = "SELECT b FROM BhUser b WHERE b.bhPass = :bhPass")})
public class BhUser implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @Basic(optional = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BH_USER_SEQ")
  @Column(name = "ID")
  private Integer id;
  @Basic(optional = false)
  @Column(name = "USER_NAME", nullable = false, unique = true)
  private String userName;
  @Basic(optional = false)
  @Column(name = "FIRST_NAME", nullable = false)
  private String firstName;
  @Basic(optional = false)
  @Column(name = "LAST_NAME", nullable = false)
  private String lastName;
  @Basic(optional = false)
  @Column(name = "BH_PASS", nullable = false)
  private String bhPass;
  @Basic(optional = false)
  @Column(name = "ACTIVE", nullable = false)
  private Integer active;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
  private List<ExamAttendance> examResultList;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
  private List<Session> sessionList;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
  private List<ExamFile> examFileList;
  @JoinColumn(name = "GROUP_ID", referencedColumnName = "ID", nullable = false)
  @ManyToOne(optional = false)
  private BhGroup group;
  @JoinColumn(name = "RIGHT_ID", referencedColumnName = "ID", nullable = false)
  @ManyToOne(optional = false)
  private Right right;
  
  
 

  public BhUser() {
  }

 
  
  public BhUser(Integer id) {
    this.id = id;
  }

  public BhUser(Integer id, String firstName, String lastName, String bhPass) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.bhPass = bhPass;
  }
  
  public BhUser(String firstName, String lastName, String bhPass) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.bhPass = bhPass;
  }

  public BhUser(String userName, String firstName, String lastName, String bhPass, 
          int active, BhGroup groupId, Right rightId) {
    this.userName = userName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.bhPass = bhPass;
    this.group = groupId;
    this.right = rightId;
    this.active = active;
  }
  
  public Integer getId() {
    return id;
  }

  public String getUsername() {
    return userName;
  }
  
  public void setId(Integer id) {
    this.id = id;
  }

  public void setUsername(String username) {
    this.userName = username;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getBhPass() {
    return bhPass;
  }

  public void setBhPass(String bhPass) {
    this.bhPass = bhPass;
  }

  public Integer getActive() {
    return active;
  }

  public void setActive(Integer active) {
    this.active = active;
  }

  public List<ExamAttendance> getExamResultList() {
    return examResultList;
  }

  public void setExamResultList(List<ExamAttendance> examResultList) {
    this.examResultList = examResultList;
  }

  public List<Session> getSessionList() {
    return sessionList;
  }

  public void setSessionList(List<Session> sessionList) {
    this.sessionList = sessionList;
  }

  public List<ExamFile> getExamFileList() {
    return examFileList;
  }

  public void setExamFileList(List<ExamFile> examFileList) {
    this.examFileList = examFileList;
  }

  public BhGroup getGroup() {
    return group;
  }

  public void setGroup(BhGroup group) {
    this.group = group;
  }

  public Right getRight() {
    return right;
  }

  public void setRight(Right right) {
    this.right = right;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof BhUser)) {
      return false;
    }
    BhUser other = (BhUser) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "model.entities.BhUser[ id=" + id + " ]";
  }
  
}
