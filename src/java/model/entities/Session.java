/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Lini
 */
@Entity
@Table(name = "SESSIONS")
@NamedQueries({
  @NamedQuery(name = "Session.findAll", query = "SELECT s FROM Session s")
  , @NamedQuery(name = "Session.findById", query = "SELECT s FROM Session s WHERE s.id = :id")
  , @NamedQuery(name = "Session.findByDtStart", query = "SELECT s FROM Session s WHERE s.dtStart = :dtStart")
  , @NamedQuery(name = "Session.findByDtEnd", query = "SELECT s FROM Session s WHERE s.dtEnd = :dtEnd")})
public class Session implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @Basic(optional = false)
  @Column(name = "ID")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SESSIONS_SEQ")
  private Long id;
//  @Basic(optional = false)
//  @Column(name = "SESSION")
//  private String session;
  @Basic(optional = false)
  @Column(name = "DT_START", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtStart;
  @Column(name = "DT_END")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dtEnd;
  @JoinColumn(name = "USER_ID", referencedColumnName = "ID", nullable = false)
  @ManyToOne(optional = false)
  private BhUser userId;

  public Session() {
  }

  public Session(Long id) {
    this.id = id;
  }

  public Session(Long id, Date dtStart) {
    this.id = id;
    this.dtStart = dtStart;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getDtStart() {
    return dtStart;
  }

  public void setDtStart(Date dtStart) {
    this.dtStart = dtStart;
  }

  public Date getDtEnd() {
    return dtEnd;
  }

  public void setDtEnd(Date dtEnd) {
    this.dtEnd = dtEnd;
  }

  public BhUser getUserId() {
    return userId;
  }

  public void setUserId(BhUser userId) {
    this.userId = userId;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Session)) {
      return false;
    }
    Session other = (Session) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "Session{" + "id=" + id + ", dtStart=" + dtStart + ", dtEnd=" + dtEnd + ", userId=" + userId + '}';
  }
  
}
