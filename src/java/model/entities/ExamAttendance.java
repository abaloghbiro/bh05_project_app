/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Lini
 */
@Entity
@Table(name = "EXAM_ATTENDANCES", uniqueConstraints={
    @UniqueConstraint(columnNames = {"USER_ID", "EXAM_ID"})
})
@NamedQueries({
  @NamedQuery(name = "ExamAttendance.findAll", query = "SELECT e FROM ExamAttendance e")
  , @NamedQuery(name = "ExamAttendance.findById", query = "SELECT e FROM ExamAttendance e WHERE e.id = :id")
  , @NamedQuery(name = "ExamAttendance.findByScoreTheory", query = "SELECT e FROM ExamAttendance e WHERE e.scoreTheory = :scoreTheory")
  , @NamedQuery(name = "ExamAttendance.findByScorePractice", query = "SELECT e FROM ExamAttendance e WHERE e.scorePractice = :scorePractice")})
public class ExamAttendance implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @Basic(optional = false)
  @Column(name = "ID")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EXAM_ATTENDANCES_SEQ")
  private Integer id;
  // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
  @Basic(optional = false)
  @Column(name = "SCORE_THEORY")
  private BigDecimal scoreTheory;
  @Basic(optional = false)
  @Column(name = "SCORE_PRACTICE")
  private BigDecimal scorePractice;
  @JoinColumn(name = "USER_ID", referencedColumnName = "ID", nullable = false)
  @ManyToOne(optional = false)
  private BhUser user;
  @JoinColumn(name = "EXAM_ID", referencedColumnName = "ID", nullable = false)
  @ManyToOne(optional = false)
  private Exam exam;

  public ExamAttendance() {
  }

  public ExamAttendance(Integer id, BigDecimal scoreTheory, BigDecimal scorePractice) {
    this.id = id;
    this.scoreTheory = scoreTheory;
    this.scorePractice = scorePractice;
  }

  public ExamAttendance(BigDecimal scoreTheory, BigDecimal scorePractice, BhUser userId, Exam exam) {
    this.scoreTheory = scoreTheory;
    this.scorePractice = scorePractice;
    this.user = userId;
    this.exam = exam;
  }

    
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public BigDecimal getScoreTheory() {
    return scoreTheory;
  }

  public void setScoreTheory(BigDecimal scoreTheory) {
    this.scoreTheory = scoreTheory;
  }

  public BigDecimal getScorePractice() {
    return scorePractice;
  }

  public void setScorePractice(BigDecimal scorePractice) {
    this.scorePractice = scorePractice;
  }

  public BhUser getUser() {
    return user;
  }

  public void setUser(BhUser user) {
    this.user = user;
  }

  public Exam getExam() {
    return exam;
  }

  public void setExam(Exam exam) {
    this.exam = exam;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof ExamAttendance)) {
      return false;
    }
    ExamAttendance other = (ExamAttendance) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "model.entities.ExamResult[ id=" + id + " ]";
  }
  
}