/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import model.AccessLevelNames;

/**
 *
 * @author Lini
 */
@Entity
@Table(name = "RIGHTS")
@NamedQueries({
  @NamedQuery(name = "Right.findAll", query = "SELECT r FROM Right r")
  , @NamedQuery(name = "Right.findById", query = "SELECT r FROM Right r WHERE r.id = :id")
  , @NamedQuery(name = "Right.findByRight", query = "SELECT r FROM Right r WHERE r.accessLevel = :accessLevel")})
public class Right implements Serializable, AccessLevelNames {

  
  private static final long serialVersionUID = 1L;
  @Id
  @Basic(optional = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RITHTS_SEQ")
  @Column(name = "ID")
  private Integer id;
  @Basic(optional = false)
  @Column(name = "ACCESS_LEVEL", unique = true, nullable = false)
  private Integer accessLevel;  
  @Basic(optional = false)
  @Column(name = "ACCESS_LEVEL_NAME", unique = true, nullable = false)
  private String accessLevelName;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "right")
  private List<BhUser> bhUserList;

  public Right() {
  }

  public Right(Integer id) {
    this.id = id;
  }

  public Right(Integer id, int accessLevel) {
    this.id = id;
    this.accessLevel = accessLevel;
    this.accessLevelName = ACCESS_LEVEL_NAMES[accessLevel].toUpperCase();
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public long getAccessLevel() {
    return accessLevel;
  }

  public void setAccessLevel(int accessLevel) {
    this.accessLevel = accessLevel;
    this.accessLevelName = ACCESS_LEVEL_NAMES[accessLevel].toUpperCase();
  }

  public String getAccessLevelName() {
    return accessLevelName;
  }
  
  public List<BhUser> getBhUserList() {
    return bhUserList;
  }

  public void setBhUserList(List<BhUser> bhUserList) {
    this.bhUserList = bhUserList;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Right)) {
      return false;
    }
    Right other = (Right) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "model.entities.Right[ id=" + id + ", accessLevel=" + accessLevel + ", accessLevelName=" + accessLevelName + " ]";
  }
  
}
