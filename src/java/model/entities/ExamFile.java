/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Lini
 */
@Entity
@Table(name = "FILES")
@NamedQueries({
  @NamedQuery(name = "ExamFile.findAll", query = "SELECT e FROM ExamFile e")
  , @NamedQuery(name = "ExamFile.findById", query = "SELECT e FROM ExamFile e WHERE e.id = :id")
  , @NamedQuery(name = "ExamFile.findByUploadTime", query = "SELECT e FROM ExamFile e WHERE e.uploadTime = :uploadTime")
  , @NamedQuery(name = "ExamFile.findByPath", query = "SELECT e FROM ExamFile e WHERE e.path = :path")})
public class ExamFile implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @Basic(optional = false)
  @Column(name = "ID")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FILES_SEQ")
  private Integer id;
  @Basic(optional = false)
  @Column(name = "UPLOAD_TIME", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date uploadTime;
  @Column(name = "PATH", nullable = false)
  private String path;
  @JoinColumn(name = "USER_ID", referencedColumnName = "ID", nullable = false)
  @ManyToOne(optional = false)
  private BhUser user;
  @JoinColumn(name = "EXAM_ID", referencedColumnName = "ID", nullable = false)
  @ManyToOne(optional = false)
  private Exam exam;

  public ExamFile() {
  }

  public ExamFile(Integer id) {
    this.id = id;
  }

  public ExamFile(Integer id, Date uploadTime) {
    this.id = id;
    this.uploadTime = uploadTime;
  }

  public ExamFile(Date uploadTime, String path, BhUser userId, Exam examId) {
    this.uploadTime = uploadTime;
    this.path = path;
    this.user = userId;
    this.exam = examId;
  }
  
  

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Date getUploadTime() {
    return uploadTime;
  }

  public void setUploadTime(Date uploadTime) {
    this.uploadTime = uploadTime;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public BhUser getUserId() {
    return user;
  }

  public void setUserId(BhUser userId) {
    this.user = userId;
  }

  public Exam getExamId() {
    return exam;
  }

  public void setExamId(Exam examId) {
    this.exam = examId;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof ExamFile)) {
      return false;
    }
    ExamFile other = (ExamFile) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "model.entities.ExamFile[ id=" + id + " ]";
  }
  
}
