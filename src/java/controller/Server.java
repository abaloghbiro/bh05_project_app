package controller;

import model.ModelInterface;
import java.util.Date;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Server {
  private static Server serverInstance = null;
  private static String fileDirectory = /*null*/"E:/BH05/TeamStarbuck/ToldACuccot2/file/examfiles/";
  
  private EntityManagerFactory emf = Persistence.createEntityManagerFactory("toldacuccot2PU");
  private ModelInterface model=null;
  
  public Server(ModelInterface model) {
    this.model = model;
    model.setEntityFactoryManager(emf);
    serverInstance = this;
  }
  
  public static Server getInstance(){
    return serverInstance;
  }
  
  public static void setFileDirectory(String path){
    fileDirectory = path;
  }
  
  public static String getFileDirectory(){
    return fileDirectory;
  }
  
  public void insertNewGroup(String groupName){
    model.insertGroup(groupName);
  }
  
  public void insertNewFile(int userID, int examID, String path){
    model.insertExamFile(new Date(), path, model.getUserByID(userID), model.getExamDateByID(examID));
  }
  
  public String getUserGroup(String appUserID){
    int id = -1;
    String groupName = null;
    try {
      id = Integer.parseInt(appUserID);
      groupName = model.getBHGroupByID(id).getName();
    } catch (NumberFormatException e) {
      //logolas ide hiba eseten
      ;
    }
    return groupName;
  }
}
