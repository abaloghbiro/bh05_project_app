/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.servlets;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AccessLevelNames;
import model.SQLUtils;
import model.beans.AdminToolsBean;
import model.beans.UserBean;

/**
 *
 * @author zolta
 */
public class AdminToolsServlet extends HttpServlet implements AccessLevelNames, SharedAttributeNames {

  private static final int PAGE_STEP = 5;
  private static final String ADMIN_TOOLS_BEAN_ATTRIBUTE_NAME = "admintoolsbean";
  private static final String USER_LIST_ATTRIBUTE_NAME = "userlist";
  
  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
   * methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
//    response.setContentType("text/html;charset=UTF-8");
//    try (PrintWriter out = response.getWriter()) {
//      /* TODO output your page here. You may use following sample code. */
//      out.println("<!DOCTYPE html>");
//      out.println("<html>");
//      out.println("<head>");
//      out.println("<title>Servlet AdminToolsServlet</title>");      
//      out.println("</head>");
//      out.println("<body>");
//      out.println("<h1>Servlet AdminToolsServlet at " + request.getContextPath() + "</h1>");
//      out.println("</body>");
//      out.println("</html>");
//    }
  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    HttpSession session = request.getSession();
    UserBean userBean = (UserBean) session.getAttribute(ControllerServlet.USER_BEAN_ATTRIBUTE_NAME);
    if(userBean != null && userBean.getAccessLevel() == ACCESS_LEVEL_ADMIN){
    String page = request.getParameter(USER_EVENT_ATTRIBUTE_NAME);
      switch (page){
        case "showpage":
          showPage(request, response, session);
          break;
        case "nextpage":
          nextPage(request, response, session);
          break;
        case "previouspage": 
          previousPage(request, response, session);
          break;
        case "toedituser":
          showEditUserPage(request, response, session);
          break;
        case "touseredited":
          showUserEditedPage(request, response, session);
          break;
        case "toinsertuser":
          showAddUserPage(request, response, session);
          break;
        case "touserinserted":
          showUserInsertedPage(request, response, session);
          break;
        case "toinsertgroup":
          showInsertGroupPage(request, response, session);
          break;
        case "togroupinserted":
          showGroupInsertedPage(request, response, session);
          break;
      }
    }
    else{
      response.sendRedirect("index.jsp");
    } 
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    HttpSession session = request.getSession();
    UserBean userBean = (UserBean) session.getAttribute(ControllerServlet.USER_BEAN_ATTRIBUTE_NAME);
    if(userBean != null && userBean.getAccessLevel() == ACCESS_LEVEL_ADMIN){
    String page = request.getParameter(USER_EVENT_ATTRIBUTE_NAME);
      switch (page){
        case "updateuser":
          updateUser(request, response, session);
          break;
        case "insertuser":
          insertUser(request, response, session);
          break;
        case "insertgroup":
          insertGroup(request, response, session);
          break;
      }
    }
    else{
      response.sendRedirect("index.jsp");
    }
  }
  
  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>

  private void showPage(HttpServletRequest request, HttpServletResponse response, HttpSession session){
    AdminToolsBean adminToolsBean = new AdminToolsBean();
    adminToolsBean.setSelecteUserIndex(0);
    adminToolsBean.setUserEditFailed(false);
    adminToolsBean.setUserCreateFailed(false);
    adminToolsBean.setUserDeleteFailed(false);
    adminToolsBean.setEditUser(false);
    adminToolsBean.setCreateUser(false);
    adminToolsBean.setDeleteUser(false);
    adminToolsBean.setMessage("");
    adminToolsBean.setFirstUserIndex(0);
    adminToolsBean.setUserList(SQLUtils.getInstance().getBhUserDTOList());
    adminToolsBean.setLastUserIndex(Math.min(adminToolsBean.getUserList().size(), PAGE_STEP));
    session.setAttribute(ADMIN_TOOLS_BEAN_ATTRIBUTE_NAME, adminToolsBean);
    try {
      request.getRequestDispatcher("WEB-INF/bhportal.jsp").forward(request, response);
    } catch (ServletException | IOException ex) {
      Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
  private void nextPage(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
    AdminToolsBean adminToolsBean = (AdminToolsBean) session.getAttribute(ADMIN_TOOLS_BEAN_ATTRIBUTE_NAME);
    if(adminToolsBean == null){
      showPage(request, response, session);
    }
    else{
      adminToolsBean.setUserEditFailed(false);
      adminToolsBean.setUserCreateFailed(false);
      adminToolsBean.setUserDeleteFailed(false);
      adminToolsBean.setEditUser(false);
      adminToolsBean.setCreateUser(false);
      adminToolsBean.setDeleteUser(false);
      int index = adminToolsBean.getFirstUserIndex() + PAGE_STEP;
      if(index < adminToolsBean.getUserList().size())
        adminToolsBean.setFirstUserIndex(index);
      adminToolsBean.setLastUserIndex(Math.min(adminToolsBean.getFirstUserIndex() + PAGE_STEP, 
              adminToolsBean.getUserList().size()));
    }
    try {
      request.getRequestDispatcher("WEB-INF/bhportal.jsp").forward(request, response);
    } catch (ServletException | IOException ex) {
      Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
    }    
  }
  
  private void previousPage(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
    AdminToolsBean adminToolsBean = (AdminToolsBean) session.getAttribute(ADMIN_TOOLS_BEAN_ATTRIBUTE_NAME);
    if(adminToolsBean == null){
      showPage(request, response, session);
    }
    else{
      adminToolsBean.setUserEditFailed(false);
      adminToolsBean.setUserCreateFailed(false);
      adminToolsBean.setUserDeleteFailed(false);
      adminToolsBean.setEditUser(false);
      adminToolsBean.setCreateUser(false);
      adminToolsBean.setDeleteUser(false);
      int index = adminToolsBean.getFirstUserIndex() - PAGE_STEP;
      if(index >= 0)
        adminToolsBean.setFirstUserIndex(index);
      adminToolsBean.setLastUserIndex(Math.min(adminToolsBean.getFirstUserIndex() + PAGE_STEP, 
              adminToolsBean.getUserList().size()));

    }
    try {
      request.getRequestDispatcher("WEB-INF/bhportal.jsp").forward(request, response);
    } catch (ServletException | IOException ex) {
      Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
    }    
  }

  private void showEditUserPage(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
    AdminToolsBean adminToolsBean = (AdminToolsBean) session.getAttribute(ADMIN_TOOLS_BEAN_ATTRIBUTE_NAME);
    if(adminToolsBean == null){
      showPage(request, response, session);
    }
    else{
      String idString = request.getParameter("uid");
      int id;
      if (idString != null && !idString.isEmpty()){
        try {
          id = Integer.parseInt(idString);
          adminToolsBean.setSelecteUserIndex(id);
          adminToolsBean.setAddGroup(false);
          adminToolsBean.setCreateUser(false);
          adminToolsBean.setEditUser(true);
          adminToolsBean.setGroupList(SQLUtils.getInstance().getGroupDTOList());
          adminToolsBean.setRightList(SQLUtils.getInstance().getRightDTOList());
          try {
            request.getRequestDispatcher("WEB-INF/bhportal.jsp").forward(request, response);
          } catch (ServletException | IOException ex) {
            adminToolsBean.setMessage("Kapcsolódás sikertelen.");
            Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
          }  
        }
        catch (NumberFormatException ex) {
          adminToolsBean.setMessage("Hibás szám formátum.");
          try {            
            request.getRequestDispatcher("WEB-INF/bhportal.jsp").forward(request, response);
          } catch (ServletException ex1) {
            Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex1);
          } catch (IOException ex1) {
            Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex1);
          }
          Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }   
      }
    }  
  }

  private void showAddUserPage(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
    AdminToolsBean adminToolsBean = (AdminToolsBean) session.getAttribute(ADMIN_TOOLS_BEAN_ATTRIBUTE_NAME);
    if(adminToolsBean == null){
      showPage(request, response, session);
    }
    else{
      adminToolsBean.setAddGroup(false);
      adminToolsBean.setCreateUser(true);
      adminToolsBean.setEditUser(false);
      adminToolsBean.setGroupList(SQLUtils.getInstance().getGroupDTOList());
      adminToolsBean.setRightList(SQLUtils.getInstance().getRightDTOList());
    } 
    try {
      request.getRequestDispatcher("WEB-INF/bhportal.jsp").forward(request, response);
    } catch (ServletException | IOException ex) {
      adminToolsBean.setMessage("Kapcsolódás sikertelen.");
      Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
    }  
  }

  private void updateUser(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
    String uidString = request.getParameter("uid");
    AdminToolsBean adminToolsBean = (AdminToolsBean) session.getAttribute(ADMIN_TOOLS_BEAN_ATTRIBUTE_NAME);
    int uid = -1;
    try {
      uid = Integer.parseInt(uidString);
    } catch (NumberFormatException ex) {
      adminToolsBean.setMessage("Felhasználó nem található.");
      try {
        response.sendRedirect("admin?userevent=showpage");
      }
      catch (IOException e) {
        Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, e);
      }        
    }
    if(uid!=-1){ //check if user id can be parsed
      String firstName = request.getParameter("firstName");
      String lastName = request.getParameter("lastName");
      String password = request.getParameter("password");
      String password1 = request.getParameter("password1");
      String group = request.getParameter("group");
      String right = request.getParameter("right");
      int active = (request.getParameter("active") == null) ? 0 : 1;
      if((password == null || password.isEmpty()|| password.length() > 5) && password.equals(password1)){
        //TODO: password policy & RegEx
        String salt = "" + System.currentTimeMillis();
        if(password != null && !password.isEmpty()){
          password = hu.bh.mv.BCrypt.hashpw(password, salt.substring(salt.length()-9,8));
        }
        else{
          password = "";
        }
        if(SQLUtils.getInstance().updateUser(uid, firstName, lastName, password, group, right, active)){
          //if user is existing
          adminToolsBean.setMessage("");
          adminToolsBean.setUserList(SQLUtils.getInstance().getBhUserDTOList());
          try {
            response.sendRedirect("admin?userevent=touseredited");
          } catch (IOException ex) {
            Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
          }
        }
        else{ //user is not in database
          adminToolsBean.setMessage("Felhasználó nem található.");
          try {
            response.sendRedirect("admin?userevent=toedituser&uid="+uid);
          }
          catch (IOException e) {
            Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, e);
          }
        }
      }
      else{ //hibás jelszó
        adminToolsBean.setMessage("A megadot jelszó nem megfelelő.");
        try {
          response.sendRedirect("admin?userevent=toedituser&uid="+uid);
        }
        catch (IOException ex) {
          Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
  }

  private void showUserEditedPage(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
    try {
      AdminToolsBean adminToolsBean = (AdminToolsBean) session.getAttribute(ADMIN_TOOLS_BEAN_ATTRIBUTE_NAME);
      adminToolsBean.setEditUser(false);
      adminToolsBean.setUserEdited(true);
      adminToolsBean.setGroupList(SQLUtils.getInstance().getGroupDTOList());
      adminToolsBean.setRightList(SQLUtils.getInstance().getRightDTOList());
      request.getRequestDispatcher("WEB-INF/bhportal.jsp").forward(request, response);
    } catch (ServletException ex) {
      Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
      Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  private void insertUser(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
    AdminToolsBean adminToolsBean = (AdminToolsBean) session.getAttribute(ADMIN_TOOLS_BEAN_ATTRIBUTE_NAME);
    String userName = request.getParameter("userName").toUpperCase();
    String firstName = request.getParameter("firstName").toUpperCase();
    String lastName = request.getParameter("lastName").toUpperCase();
    String password = request.getParameter("password");
    String password1 = request.getParameter("password1");
    String group = request.getParameter("group");
    String right = request.getParameter("right");
    int active = (request.getParameter("active") == null) ? 0 : 1;
    if(password != null && !password.isEmpty() && password.length() > 5 && password.equals(password1)){
      //TODO: password policy & RegEx
      String salt = "" + System.currentTimeMillis();
      
      //TODO: generate proper salt for hashing
      password = hu.bh.mv.BCrypt.hashpw(password, hu.bh.mv.BCrypt.gensalt(8));
      if(SQLUtils.getInstance().insertUser(userName, firstName, lastName, password, active, group, right)){
        synchronized(adminToolsBean){
          adminToolsBean.setMessage("A felhasználó felvitele sikeres volt.");
          adminToolsBean.setUserList(SQLUtils.getInstance().getBhUserDTOList());
        }
      }
      else{
        adminToolsBean.setMessage("A felhasználó felvitele sikertelen.");
        try {        
          response.sendRedirect("admin?userevent=toinsertuser");
        } catch (IOException ex) {
          Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }        
      }
    }
    else{
      adminToolsBean.setMessage("A megadot jelszó nem megfelelő.");
      try {        
        response.sendRedirect("admin?userevent=toinsertuser");
      } catch (IOException ex) {
        Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    try {
      response.sendRedirect("admin?userevent=touserinserted");
    } catch (IOException ex) {
      Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  private void showUserInsertedPage(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
    try {
      AdminToolsBean adminToolsBean = (AdminToolsBean) session.getAttribute(ADMIN_TOOLS_BEAN_ATTRIBUTE_NAME);
      adminToolsBean.setCreateUser(false);
      adminToolsBean.setUserCreated(true);
      adminToolsBean.setGroupList(SQLUtils.getInstance().getGroupDTOList());
      adminToolsBean.setRightList(SQLUtils.getInstance().getRightDTOList());
      request.getRequestDispatcher("WEB-INF/bhportal.jsp").forward(request, response);
    } catch (ServletException ex) {
      Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
      Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  private void showInsertGroupPage(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
    AdminToolsBean adminToolsBean = (AdminToolsBean) session.getAttribute(ADMIN_TOOLS_BEAN_ATTRIBUTE_NAME);
    adminToolsBean.setAddGroup(true);
    adminToolsBean.setCreateUser(false);
    adminToolsBean.setEditUser(false);
    try {
      request.getRequestDispatcher("WEB-INF/bhportal.jsp").forward(request, response);
    } catch (ServletException ex) {
      Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
      Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  private void insertGroup(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
    AdminToolsBean adminToolsBean = (AdminToolsBean) session.getAttribute(ADMIN_TOOLS_BEAN_ATTRIBUTE_NAME);
    String groupName = request.getParameter("groupname").toUpperCase();
    if(SQLUtils.getInstance().insertGroup(groupName)){
      adminToolsBean.setMessage("");
      try {
        response.sendRedirect("admin?userevent=togroupinserted");
      } catch (IOException ex) {
        Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    else{
      adminToolsBean.setMessage("A csoport felvitele sikertelen.");
      try {
        response.sendRedirect("admin?userevent=toinsertgroup");
      } catch (IOException ex) {
        Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
      }
    }

  }

  private void showGroupInsertedPage(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
    AdminToolsBean adminToolsBean = (AdminToolsBean) session.getAttribute(ADMIN_TOOLS_BEAN_ATTRIBUTE_NAME);
    adminToolsBean.setAddGroup(false);
    try {
      request.getRequestDispatcher("WEB-INF/bhportal.jsp").forward(request, response);
    } catch (ServletException ex) {
      Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
      Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
}