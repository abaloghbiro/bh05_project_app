/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.servlets;

import static controller.servlets.ControllerServlet.USER_EVENT_ATTRIBUTE_NAME;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AccessLevelNames;
import model.SQLUtils;
import model.beans.UserBean;

/**
 *
 * @author zolta
 */
@WebServlet(name = "LogInServlet", urlPatterns = {"/login"})
public class LogInServlet extends HttpServlet implements SharedAttributeNames{

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    ;
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    String event = request.getParameter(USER_EVENT_ATTRIBUTE_NAME);
    if("login".equals(event)){
      login(request, response);
    }
    else{
      response.sendRedirect("index.jsp");
    }
    ;
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>

    private void login(HttpServletRequest request, HttpServletResponse response) {
    String userName = request.getParameter("username");
    String password = request.getParameter("password");
    UserBean bean = ControllerServlet.autentication(userName, password);
    try {
      HttpSession session = request.getSession();
      session.setAttribute(USER_BEAN_ATTRIBUTE_NAME, bean);
      if(bean.getAccessLevel() == AccessLevelNames.ACCESS_LEVEL_ADMIN)
        response.sendRedirect("admin?userevent=showpage");
      else if(bean.getAccessLevel() == AccessLevelNames.ACCESS_LEVEL_STUDENT)
				response.sendRedirect("ExamServlet");
			else if (bean.getAccessLevel() == AccessLevelNames.ACCESS_LEVEL_INSTRUCTOR)
        response.sendRedirect("instructor");
      else
        response.sendRedirect("index.jsp");
    } 
    catch (IOException ex) {
      Logger.getLogger(ControllerServlet.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
}