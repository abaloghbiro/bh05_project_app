package controller.servlets;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.SQLUtils;
import model.beans.UserBean;
import model.dtos.BhUserDTO;
import model.dtos.ExamAttendanceDTO;
import model.entities.BhGroup;
import model.entities.BhUser;
import model.entities.Right;

@WebServlet(name = "ControllerServlet", urlPatterns = {"/bhservice"})
public class ControllerServlet extends HttpServlet implements SharedAttributeNames{

    private static SQLUtils model;

    private static final String MODEL_ATTRIBUTE_NAME = "model";

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String event = request.getParameter(USER_EVENT_ATTRIBUTE_NAME);
        if (null != event) {
            switch (event) {
                case "logout":
                    logout(request, response);
                    break;
                default: //error page
            }
        } else {
            //error page
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        //every post has to contain a hidden parameter named as "sender"
        String event = request.getParameter(USER_EVENT_ATTRIBUTE_NAME);
        if (null != event) {
            switch (event) {
                case "login":
                    //login(request, response);
                    break;

                case "register":
                    register(request, response);
                    break;

                case "update":
                    register(request, response);
                    break;

                case "passwordchange":
                    changePassword(request, response);
                    break;
                default: //error page

                case "newExamDate":
                    changePassword(request, response);
                    break;
     

            }
        } else {
            //error page
        }
    }



    private void toMainPage(HttpServletRequest request, HttpServletResponse response) {
        UserBean userBean = (UserBean) request.getSession().getAttribute(USER_BEAN_ATTRIBUTE_NAME);
        if (userBean != null) {
            toMainPage(userBean, response);
        }
    }

    private void toMainPage(UserBean userBean, HttpServletResponse response) {
      try {
        switch (userBean.getAccessLevel()) {
            case UserBean.ACCESS_LEVEL_STUDENT:
                response.sendRedirect("studentexamlist.jsp");
                break;
            case UserBean.ACCESS_LEVEL_INSTRUCTOR:
                response.sendRedirect("instructoraddexam.jsp");
                break;
            case UserBean.ACCESS_LEVEL_ADMIN:
              response.sendRedirect("admin.jsp");
              break;
            default:
              break;
          }
        } catch (IOException ex) {
            Logger.getLogger(ControllerServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void changePassword(HttpServletRequest request, HttpServletResponse response) {
        UserBean userBean = (UserBean) request.getSession().getAttribute(USER_BEAN_ATTRIBUTE_NAME);
        if (userBean != null) {
            String oldPassword = request.getParameter("oldpassword");
            String newPasswordFirst = request.getParameter("newpasswordfirst");
            String newPasswordSecond = request.getParameter("newpasswordsecond");
            if (oldPassword != null && oldPassword != "" && newPasswordFirst != null
                    && //TODO: jelsz� valid�l�s
                    newPasswordFirst.equals(newPasswordSecond)) {
                //TODO: jelsz� fel�l�r�s
                model.updatePassword(0, newPasswordFirst);
//        userBean.setPasswordChangeFailed(false);
//        userBean.setPasswordChanged(true);
            } else {
//        userBean.setPasswordChangeFailed(true);
            }
        }
    }

    private void logout(HttpServletRequest request, HttpServletResponse response) {
        request.getSession().removeAttribute(USER_BEAN_ATTRIBUTE_NAME);
        try {
            response.sendRedirect("index.jsp");
        } catch (IOException ex) {
            Logger.getLogger(ControllerServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void register(HttpServletRequest request, HttpServletResponse response) {
        UserBean userBean = (UserBean) request.getSession().getAttribute(USER_BEAN_ATTRIBUTE_NAME);
        if (userBean != null) {
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String userName = request.getParameter("userName");
            String bhPass = request.getParameter("password");
            String groupId = request.getParameter("group");
            String rightId = request.getParameter("right");
            String email = request.getParameter("email");
            String active = request.getParameter("active");


            BhGroup bhGroup = new BhGroup();
            bhGroup.setName(groupId);
            Right right = new Right();
            right.setAccessLevel(Integer.parseInt(rightId));
            
             BhUser user = new BhUser(firstName, lastName,userName,  bhPass,0, bhGroup, right);
            
            model.insertUser(user);
           
            
//            if (d)) {
//                //TODO: jelsz� fel�l�r�s
////        userBean.setPasswordChangeFailed(false);
////        userBean.setPasswordChanged(true);
//            } else {
////        userBean.setPasswordChangeFailed(true);
//            }
        }

    }

		public static UserBean autentication(String userName, String password){
			userName = userName.toUpperCase();
			BhUserDTO user = SQLUtils.getInstance().getUserByUserName(userName);
			if(null==user || 0==user.getActive())
				return new UserBean("invalid", "invalid", 0);
			else
				if(hu.bh.mv.BCrypt.checkpw(password, user.getBhPass()))
					return new UserBean(user.getUserName(), user.getFirstName()+" "+user.getLastName(),(int) user.getRightDTO().getAccessLevel());
				else 
				return new UserBean("invalid", "invalid", 0);
  }
		
    private void update(HttpServletRequest request, HttpServletResponse response) {
        UserBean userBean = (UserBean) request.getSession().getAttribute(USER_BEAN_ATTRIBUTE_NAME);
        if (userBean != null) {
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String userName = request.getParameter("userName");
            String bhPass = request.getParameter("password");
            String group = request.getParameter("group");
            String right = request.getParameter("right");
            String email = request.getParameter("email");
            String active = request.getParameter("active");
             
//            if (d)) {
//                //TODO: jelsz� fel�l�r�s
////        userBean.setPasswordChangeFailed(false);
////        userBean.setPasswordChanged(true);
//            } else {
////        userBean.setPasswordChangeFailed(true);
//            }
        }

    }
    
     private void newExamDate(HttpServletRequest request, HttpServletResponse response) {
        UserBean userBean = (UserBean) request.getSession().getAttribute(USER_BEAN_ATTRIBUTE_NAME);
        if (userBean != null) {
         
                    
        }
     }

		 
		 	private List<ExamAttendanceDTO> getExamResultDao(String userName){
		return model.getExamResults(userName);
	}
	
	private List<Integer> getExamIdsByExamDate (Date date) {
		return model.getExamIdByExamDate(date);
	}
}
