package controller.servlets;

import controller.Server;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.SQLUtils;

@WebServlet(name = "UploadServlet", urlPatterns = {"/upload-file"})
@MultipartConfig
public class UploadServlet extends HttpServlet {

  private static final long serialVersionUID = 102831973239L;
  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
   *
   * @param req
   * @param resp
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  
  
  @Override
  protected void service(HttpServletRequest req, HttpServletResponse resp)
          throws ServletException, IOException {
//    HttpSession session = req.getSession();
//    AppUserBean appUserBean;
//    appUserBean = (AppUserBean) session.getAttribute("appBean");
//    if (null == appUserBean){
//      appUserBean = new AppUserBean();
//      session.setAttribute("appBean", appUserBean);
//    }
    super.service(req, resp);
  }

  protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
      /* TODO output your page here. You may use following sample code. */
      out.println("<!DOCTYPE html>");
      out.println("<html>");
      out.println("<head>");
      out.println("<title>Servlet UploadServlet</title>");      
      out.println("</head>");
      out.println("<body>");
      out.println("<h1>Servlet UploadServlet at " + request.getContextPath() + "</h1>");
      out.println("</body>");
      out.println("</html>");
    }
  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    //processRequest(request, response);
    getServletContext().getRequestDispatcher("/WEB-INF/uploadfile.jsp").forward(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
     HttpSession session = request.getSession();
    StringBuffer fileName = new StringBuffer();
            
    fileName.append(request.getParameter("examID")).append("/");
    fileName.append(session.getAttribute("name")).append(".zip");
    
    File file = new File(Server.getFileDirectory()+fileName);
    Path path = file.toPath();
    Part filePart = request.getPart("uploadedfile");
    
    try(InputStream in = filePart.getInputStream()){
      Files.copy(in, path, REPLACE_EXISTING);
      int userID = SQLUtils.getInstance().getUserByUserName((String)session.getAttribute("username")).getId();
      List<Integer> examIDs = SQLUtils.getInstance().getExamIdByExamDate((Date)session.getAttribute("examID"));
      Server.getInstance().insertNewFile(userID, examIDs.get(examIDs.size()-1), path.toString());
    }
    catch(IOException e){
      //e.printStackTrace();
    }
		response.sendRedirect("/ExamServlet");
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>

}
