/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.servlets;

import static controller.servlets.SharedAttributeNames.USER_EVENT_ATTRIBUTE_NAME;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AccessLevelNames;
import model.beans.InstructorBean;
import model.beans.UserBean;

/**
 *
 * @author akisreti
 */
public class InstructorServlet extends HttpServlet implements AccessLevelNames {
  
  private static final String INSTRUCTOR_TOOLS_BEAN_ATTRIBUTE_NAME = "instructortoolsbean";
  
  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    int displayPageParam = 1;
    if (request.getParameter("displayPage") != null)
      displayPageParam = Integer.parseInt(request.getParameter("displayPage"));
    HttpSession session = request.getSession();
    UserBean userBean = (UserBean) session.getAttribute(ControllerServlet.USER_BEAN_ATTRIBUTE_NAME);
    if (userBean != null) {
      InstructorBean instructorBean = new InstructorBean();
      //instructorBean.setPage(1);
      switch (displayPageParam) {
        case 1: instructorBean.setPage(1);
          break;
        case 2: instructorBean.setPage(2);
          break;
        case 3: instructorBean.setPage(3);
          break;
        case 4: instructorBean.setPage(4);
          break;
        default: instructorBean.setPage(1);
          break;
      }
      session.setAttribute("displayPage", instructorBean);
      try {
        request.getRequestDispatcher("WEB-INF/instructor.jsp").forward(request, response);
        //response.sendRedirect("bhportal.jsp");
      }
      catch (ServletException | IOException ex) {
        Logger.getLogger(AdminToolsServlet.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    else {
      response.sendRedirect("error.jsp");
    }
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>

}
