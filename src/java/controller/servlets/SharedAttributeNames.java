package controller.servlets;

public interface SharedAttributeNames {
    public static final String USER_EVENT_ATTRIBUTE_NAME = "userevent";
    public static final String USER_BEAN_ATTRIBUTE_NAME = "userbean";
}
