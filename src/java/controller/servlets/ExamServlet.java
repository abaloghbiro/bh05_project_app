package controller.servlets;

import static controller.servlets.SharedAttributeNames.USER_BEAN_ATTRIBUTE_NAME;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.beans.UserBean;
import model.SQLUtils;
import model.dtos.ExamAttendanceDTO;

public class ExamServlet extends HttpServlet {
  
  private List<ExamAttendanceDTO> exams = new ArrayList<>();

  
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
      out.println("<!DOCTYPE html>");
      out.println("<html>");
      out.println("<head>");
      out.println("<title>Servlet ExamServlet</title>");      
      out.println("</head>");
      out.println("<body>");
      out.println("<h1>Servlet ExamServlet at " + request.getContextPath() + "</h1>");
      out.println("</body>");
      out.println("</html>");
    }
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    UserBean userBean = (UserBean) request.getSession().getAttribute(USER_BEAN_ATTRIBUTE_NAME);
		exams = SQLUtils.getInstance().getExamResults(userBean.getUserName());
    request.setAttribute("exams", exams);
		request.setAttribute("isIt", model.SQLUtils.getInstance().isItExamTime(new Date(System.currentTimeMillis())));
		request.setAttribute("accessLevel", userBean.getAccessLevel());
		request.getRequestDispatcher("WEB-INF/exams.jsp").forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>

}
